# Roman Gordon - Programming 3 Project - 2023-24
Name: Roman Gordon

## Entity Overview
My 3 entities are:
- Locations
- Tree Species
- Tree

Locations and Tree Species have a many-to-many relationship (each location can have many species and each species can be in many locations)

Location and Tree have a one-to-many relationship (each location can have many trees but each tree can only have 1 location)

Tree Species and Tree have a one-to-many relationship (each species can have many trees but each tree can only have 1 species)


## Profile and Database Explanation
There are 4 repository profiles:
- "jparepository" uses the JPARepository implementation for the repository alongside a service for it (Week 10)
- "jpadatabase" uses the Entity Manager from JPA in the repository for database access (Week 9)
- "jdbcdatabase" uses JdbcTemplates in order to access the database (Week 7)
- "javacollections" uses a DataFactory class with the repository in order to create and store the data (Week 1)

There are 2 database profiles:
- "dev" for an H2 memory database
- "prod" for a postgresql database

## More Info
The configuration for the postgresql database credentials can be found in application-prod.properties

The profiles need to be set in application.properties. Choose one of the repository profiles and 1 of the database profiles at the same time
