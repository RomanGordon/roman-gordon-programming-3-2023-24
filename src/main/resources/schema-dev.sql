create table tree_species
(
    species_id INTEGER auto_increment,
    species_name            CHARACTER VARYING(30) not null,
    leaf_type               ENUM('CONIFER', 'BROADLEAF', 'SCALE_LEAF', 'LOBED', 'TOOTHED', 'ENTIRE') not null,
    conservation_status     ENUM('EXTINCT', 'THREATENED', 'POSSIBLY_THREATENED', 'NOT_THREATENED', 'NOT_EVALUATED') not null,
    bears_fruit             BOOLEAN not null,
    constraint "tree_species_pk"
        primary key (species_id)
);

create table location
(
    location_id INTEGER auto_increment,
    country                     CHARACTER VARYING(30) not null,
    sub_climates                ENUM('TUNDRA', 'SUBTROPICAL', 'TEMPERATE', 'TROPICAL', 'POLAR', 'ALPINE', 'DESERT', 'MARITIME', 'CONTINENTAL', 'MARS') ARRAY not null,
    yearly_average_rainfall     DOUBLE not null,
    percentage_of_forestland    INTEGER not null,
    constraint "locations_pk"
        primary key (location_id)
);

create table species_location_relationship
(
    species_id INTEGER not null,
    location_id INTEGER not null,
    FOREIGN KEY (species_id) references tree_species(species_id),
    FOREIGN KEY (location_id) references location(location_id),
    constraint "relationship_pk"
        primary key (species_id, location_id)
);

create table tree
(
    tree_id INTEGER auto_increment,
    planting_date DATE not null,
    height DOUBLE not null,
    species_id INTEGER not null,
    location_id INTEGER not null,
    FOREIGN KEY (species_id) references tree_species(species_id),
    FOREIGN KEY (location_id) references location(location_id),
    constraint "tree_pk"
        primary key (tree_id)
);