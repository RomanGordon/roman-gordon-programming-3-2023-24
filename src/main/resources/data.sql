INSERT INTO tree_species (species_name, leaf_type, conservation_status, bears_fruit) VALUES ('Aspen', 'LOBED', 'POSSIBLY_THREATENED', false);
INSERT INTO tree_species (species_name, leaf_type, conservation_status, bears_fruit) VALUES ('Alder', 'TOOTHED','NOT_THREATENED',true);
INSERT INTO tree_species (species_name, leaf_type, conservation_status, bears_fruit) VALUES ('Fir', 'CONIFER', 'THREATENED',true);
INSERT INTO tree_species (species_name, leaf_type, conservation_status, bears_fruit) VALUES ('Cherry', 'TOOTHED', 'THREATENED', false);
INSERT INTO tree_species (species_name, leaf_type, conservation_status, bears_fruit) VALUES ('Chestnut', 'ENTIRE', 'NOT_EVALUATED', true);
INSERT INTO tree_species (species_name, leaf_type, conservation_status, bears_fruit) VALUES ('Cypress', 'CONIFER', 'POSSIBLY_THREATENED',  true);
INSERT INTO tree_species (species_name, leaf_type, conservation_status, bears_fruit) VALUES ('Elm', 'SCALE_LEAF', 'EXTINCT', false);
INSERT INTO tree_species (species_name, leaf_type, conservation_status, bears_fruit) VALUES ('Hazel', 'BROADLEAF', 'THREATENED', false);
INSERT INTO tree_species (species_name, leaf_type, conservation_status, bears_fruit) VALUES ('Oak', 'BROADLEAF', 'NOT_THREATENED', true);
INSERT INTO tree_species (species_name, leaf_type, conservation_status, bears_fruit) VALUES ('Maple', 'SCALE_LEAF', 'THREATENED', true);

INSERT INTO location (country, sub_climates, yearly_average_rainfall, percentage_of_forestland) VALUES ('Belgium', ARRAY ['TEMPERATE', 'MARITIME'], 986.4, 23);
INSERT INTO location (country, sub_climates, yearly_average_rainfall, percentage_of_forestland) VALUES ('Austria', ARRAY ['ALPINE', 'TEMPERATE'], 1090.8, 47);
INSERT INTO location (country, sub_climates, yearly_average_rainfall, percentage_of_forestland) VALUES ('Switzerland', ARRAY ['TEMPERATE', 'ALPINE'], 1289.05, 32);
INSERT INTO location (country, sub_climates, yearly_average_rainfall, percentage_of_forestland) VALUES ('Czechia', ARRAY ['TEMPERATE', 'TUNDRA'], 450.5, 35);
INSERT INTO location (country, sub_climates, yearly_average_rainfall, percentage_of_forestland) VALUES ('Hungary', ARRAY ['CONTINENTAL'], 557.0, 23);
INSERT INTO location (country, sub_climates, yearly_average_rainfall, percentage_of_forestland) VALUES ('Poland', ARRAY ['MARITIME', 'CONTINENTAL'], 643.21, 31);
INSERT INTO location (country, sub_climates, yearly_average_rainfall, percentage_of_forestland) VALUES ('Portugal', ARRAY ['MARITIME', 'SUBTROPICAL'], 796.84, 36);
INSERT INTO location (country, sub_climates, yearly_average_rainfall, percentage_of_forestland) VALUES ('Denmark', ARRAY ['TEMPERATE', 'ALPINE', 'POLAR'], 746.0, 16);
INSERT INTO location (country, sub_climates, yearly_average_rainfall, percentage_of_forestland) VALUES ('France', ARRAY ['ALPINE', 'TUNDRA'], 836.54, 32);
INSERT INTO location (country, sub_climates, yearly_average_rainfall, percentage_of_forestland) VALUES ('Liechtenstein', ARRAY ['CONTINENTAL', 'ALPINE'], 1000.0, 42);

INSERT INTO species_location_relationship (species_id, location_id) VALUES (2, 1);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (3, 1);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (10, 1);

INSERT INTO species_location_relationship (species_id, location_id) VALUES (6, 2);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (2, 2);

INSERT INTO species_location_relationship (species_id, location_id) VALUES (4, 3);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (10, 3);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (1, 3);

INSERT INTO species_location_relationship (species_id, location_id) VALUES (3, 4);

INSERT INTO species_location_relationship (species_id, location_id) VALUES (10, 5);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (2, 5);

INSERT INTO species_location_relationship (species_id, location_id) VALUES (6, 6);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (9, 6);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (4, 6);

INSERT INTO species_location_relationship (species_id, location_id) VALUES (5, 7);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (8, 7);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (2, 7);

INSERT INTO species_location_relationship (species_id, location_id) VALUES (4, 8);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (2, 8);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (1, 8);

INSERT INTO species_location_relationship (species_id, location_id) VALUES (1, 9);

INSERT INTO species_location_relationship (species_id, location_id) VALUES (8, 10);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (9, 10);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (5, 10);
INSERT INTO species_location_relationship (species_id, location_id) VALUES (4, 10);


INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1982-09-01', 14.71, 1, 3);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2015-08-20', 18.5, 1, 3);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2016-03-08', 11.31, 1, 3);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1974-08-20', 16.58, 1, 3);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1985-04-17', 7.69, 1, 3);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1968-10-01', 14.5, 1, 8);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1990-08-09', 9.67, 1, 8);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1951-05-20', 19.45, 1, 8);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2016-06-12', 16.28, 1, 9);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1974-02-20', 20.54, 1, 9);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1966-05-07', 10.37, 1, 9);

INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1963-05-22', 22.83, 2, 1);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1952-03-18', 26.42, 2, 1);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1952-05-02', 18.15, 2, 1);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2012-01-14', 22.3, 2, 1);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2020-06-18', 9.61, 2, 2);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1984-06-20', 25.29, 2, 2);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1963-03-04', 19.95, 2, 5);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1950-04-18', 26.15, 2, 5);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1962-08-17', 21.47, 2, 5);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1966-04-19', 22.13, 2, 7);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1982-01-08', 25.89, 2, 7);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1956-10-20', 18.77, 2, 7);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1987-10-08', 22.81, 2, 7);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2006-01-18', 19.39, 2, 8);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1966-06-02', 19.25, 2, 8);

INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2018-04-21', 10.34, 3, 1);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1963-08-07', 34.64, 3, 1);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1989-07-04', 59.73, 3, 1);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2018-04-07', 10.14, 3, 4);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1995-03-09', 64.87, 3, 4);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1997-02-03', 54.72, 3, 4);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1969-11-07', 71.99, 3, 4);

INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2008-03-10', 7.3, 4, 3);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1955-07-13', 9.49, 4, 3);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2007-10-13', 8.18, 4, 3);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1981-07-22', 9.4, 4, 3);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1950-02-09', 11.12, 4, 6);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1966-01-04', 11.14, 4, 6);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1959-04-11', 9.08, 4, 8);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2006-07-14', 6.07, 4, 8);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1966-11-08', 7.52, 4, 8);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1953-09-01', 6.44, 4, 10);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1956-08-25', 7.26, 4, 10);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1988-11-05', 11.72, 4, 10);

INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2007-11-21', 16.95, 5, 7);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1975-11-22', 20.35, 5, 7);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1979-03-25', 31.46, 5, 7);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2000-01-17', 28.85, 5, 10);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1990-11-06', 25.76, 5, 10);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1960-04-16', 24.97, 5, 10);

INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1961-04-22', 9.31, 6, 2);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1997-08-27', 12.43, 6, 2);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1972-07-08', 16.56, 6, 6);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1990-01-18', 12.36, 6, 6);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1972-01-18', 21.85, 6, 6);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2021-09-20', 6.94, 6, 6);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2017-06-26', 8.18, 6, 6);

INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2007-01-27', 9.58, 8, 7);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2002-04-01', 8.19, 8, 10);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1977-05-03', 11.94, 8, 10);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1990-09-24', 11.34, 8, 10);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1953-10-15', 11.95, 8, 10);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('2006-10-26', 8.38, 8, 10);

INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1962-01-13', 27.62, 9, 6);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1986-10-23', 23.01, 9, 6);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1989-09-09', 14.36, 9, 6);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1963-07-17', 26.66, 9, 10);

INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1960-09-19', 31.88, 10, 1);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1986-11-19', 19.67, 10, 3);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1986-03-25', 23.02, 10, 3);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1997-05-27', 43.37, 10, 3);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1975-10-18', 12.97, 10, 5);
INSERT INTO tree (planting_date, height, species_id, location_id) VALUES ('1961-01-07', 30.77, 10, 5);
