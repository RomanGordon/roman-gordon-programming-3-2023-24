drop table if exists species_location_relationship;
drop table if exists tree;
drop table if exists location;
drop table if exists tree_species;

create table tree_species
(
    species_id              SERIAL,
    species_name            CHARACTER VARYING(30) not null,
    leaf_type               VARCHAR(10) not null CHECK ( leaf_type IN ('CONIFER', 'BROADLEAF', 'SCALE_LEAF', 'LOBED', 'TOOTHED', 'ENTIRE')),
    conservation_status     VARCHAR(19) not null CHECK ( conservation_status IN ('EXTINCT', 'THREATENED', 'POSSIBLY_THREATENED', 'NOT_THREATENED', 'NOT_EVALUATED') ),
    bears_fruit             BOOLEAN not null,
    constraint "tree_species_pk"
        primary key (species_id)
);

create table location
(
    location_id                 SERIAL,
    country                     CHARACTER VARYING(30) not null,
    sub_climates                VARCHAR(11)[] not null,
    yearly_average_rainfall     NUMERIC(10,4) not null,
    percentage_of_forestland    INTEGER not null,
    constraint "locations_pk"
        primary key (location_id),
    CONSTRAINT sub_climates_check CHECK (ARRAY[sub_climates]::VARCHAR(11)[] <@ ARRAY['TUNDRA', 'SUBTROPICAL', 'TEMPERATE', 'TROPICAL', 'POLAR', 'ALPINE', 'DESERT', 'MARITIME', 'CONTINENTAL', 'MARS']::VARCHAR(11)[])
);

create table species_location_relationship
(
    species_id INTEGER not null,
    location_id INTEGER not null,
    FOREIGN KEY (species_id) references tree_species(species_id),
    FOREIGN KEY (location_id) references location(location_id),
    constraint "relationship_pk"
        primary key (species_id, location_id)
);

create table tree
(
    tree_id SERIAL,
    planting_date DATE not null,
    height NUMERIC(10,4) not null,
    species_id INTEGER not null,
    location_id INTEGER not null,
    FOREIGN KEY (species_id) references tree_species(species_id),
    FOREIGN KEY (location_id) references location(location_id),
    constraint "tree_pk"
        primary key (tree_id)
);