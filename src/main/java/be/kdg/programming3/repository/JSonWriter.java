package be.kdg.programming3.repository;

import be.kdg.programming3.util.LocalDateSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Component
public class JSonWriter {

    public void saveToJson(List objectList, String name){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();

        String jsonString = gson.toJson(objectList);

        try (FileWriter jsonWriter = new FileWriter(".\\src\\main\\resources\\"+ name +".json")) {
            jsonWriter.write(jsonString);
//            if(!Objects.equals(name, "trees")){
            System.out.println("Data is saved to " + name + ".json...");
//            }
        } catch (IOException e) {
            System.out.println("Unable to save " + name + "to json");
            System.out.println(e.getMessage());
        }
    }
}
