package be.kdg.programming3.repository.locations;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.TreeSpecies;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;

// Using JPA queries
@Repository
@Profile("jpadatabase")
public class JPALocationsRepository implements LocationsRepository{
    Logger logger = LoggerFactory.getLogger(JPALocationsRepository.class);
    @PersistenceContext
    private EntityManager em;
    @Override
    @Transactional
    public Locations createLocation(Locations location) {
        em.persist(location);
        logger.debug("Location " + location.getCountry() + " created successfully");
        return location;
    }

    @Override
    public Locations addLocationSpecies(Locations location, List<TreeSpecies> species) {
        species.forEach(location::addSpecies);
        em.persist(location);
        logger.debug("Added species to " + location.getCountry());
        return location;
    }

    @Override
    @Transactional
    public List<Locations> readLocations() {
        logger.debug("Finding all locations");
        return em.createQuery("SELECT location FROM Locations location", Locations.class).getResultList();
    }

    @Override
    @Transactional
    public Locations getLocationByName(String countryName){
        logger.debug("Finding location named " + countryName);
        Query query = em.createQuery("SELECT location FROM Locations location WHERE LOWER(location.country) = ?1", Locations.class);
        query.setParameter(1, countryName.toLowerCase());
        return (Locations) query.getSingleResult();
    }

    @Override
    public List<Locations> findWhereMinForestCoverage(int forestCoverage) {
        logger.debug("Querying database for all locations where forest coverage is above " + forestCoverage);
        Query query = em.createQuery("SELECT location FROM Locations location WHERE location.percentageOfForestland >= :forest", Locations.class);
        query.setParameter("forest", forestCoverage);
        return query.getResultList();
    }

    @Override
    public List<Locations> findWhereRainfallBelow(double rainfall) {
        logger.debug("Querying database for all locations where rainfall is above " + rainfall);
        Query query = em.createQuery("SELECT location FROM Locations location WHERE location.yearlyAverageRainfall <= :rain", Locations.class);
        query.setParameter("rain", rainfall);
        return query.getResultList();
    }

    @Override
    public List<Locations> findWhereNumberOfSpeciesAbove(int number) {
        logger.debug("Querying database for all locations where there are more than " + number + " species");
        Query query = em.createQuery("SELECT location FROM Locations location WHERE location.speciesFound.size >= :species", Locations.class);
        query.setParameter("species", number);
        return query.getResultList();
    }

    @Override
    @Transactional
    public void removeLocation(String countryName) {
        logger.debug("Removing " + countryName);
        Query query = em.createQuery("SELECT location FROM Locations location WHERE LOWER(location.country) = ?1", Locations.class);
        query.setParameter(1, countryName.toLowerCase());
        Locations location = (Locations) query.getSingleResult();

        for (TreeSpecies species : location.getSpeciesFound()) {
            species.getLocationsFoundIn().remove(location);
        }

        location.getSpeciesFound().clear();
        em.remove(location);
    }

    @Override
    public void updateLocation(Locations location) {
        em.merge(location);
    }
}
