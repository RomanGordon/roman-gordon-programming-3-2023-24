package be.kdg.programming3.repository.locations;

import be.kdg.programming3.domain.Locations;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Profile("jparepository")
public interface LocationsJPARepository extends JpaRepository<Locations, Integer> {
    List<Locations> findByCountryEqualsIgnoreCase(String country);
    void deleteByCountryEqualsIgnoreCase(String country);
    List<Locations> findByPercentageOfForestlandGreaterThanEqual(int coverage);
    List<Locations> findByYearlyAverageRainfallLessThanEqual(double rainfall);
    List<Locations> findByYearlyAverageRainfallGreaterThanEqual(double rainfall);
    List<Locations> findByPercentageOfForestlandGreaterThanEqualAndYearlyAverageRainfallGreaterThanEqual(int coverage, double rainfall);
    @Query("SELECT l FROM Locations l WHERE SIZE(l.speciesFound) >= :minSpecies")
    List<Locations> findWhereMinimumTreeSpecies(int minSpecies);
}
