package be.kdg.programming3.repository.locations;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.Locations.*;
import be.kdg.programming3.domain.Tree;
import be.kdg.programming3.domain.TreeSpecies;
import be.kdg.programming3.domain.TreeSpecies.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Array;
import java.sql.SQLException;
import java.util.*;

// Using JDBC templates
@Repository
@Profile("jdbcdatabase")
public class JDBCLocationsRepository implements  LocationsRepository{
    private final Logger logger = LoggerFactory.getLogger(LocationsRepository.class);
    public static List<Locations> locationsList = new ArrayList<>();
    JdbcTemplate jdbcTemplate;

    public JDBCLocationsRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Locations createLocation(Locations location) {
        String[] subClimates = Arrays.stream(location.getSubClimates()).map(Enum::name).toArray(String[]::new);

        jdbcTemplate.update("INSERT INTO location (country, sub_climates, yearly_average_rainfall, percentage_of_forestland) VALUES (?, ?, ?, ?)",
                location.getCountry(), subClimates, location.getYearlyAverageRainfall(),
                location.getPercentageOfForestland());

        logger.debug("Location " + location.getCountry() + " created successfully");
        return location;
    }


    @Override
    public Locations addLocationSpecies(Locations location, List<TreeSpecies> species) {
        species.forEach(treeSpecies -> {
            if (jdbcTemplate.query("SELECT * FROM location WHERE country = ?", (rs, rowNum) -> rs.getString("country"), location.getCountry()).isEmpty()){
                createLocation(location);
            }

            List<Integer> relationships = jdbcTemplate.query("SELECT * FROM species_location_relationship WHERE species_id = (SELECT species_id FROM tree_species WHERE species_name = ? LIMIT 1)",
                    (rs, rowNum) -> rs.getInt("location_id") , treeSpecies.getSpeciesName());

            if (!relationships.contains(location.getLocationId()) &&
                    !jdbcTemplate.query("SELECT * FROM tree_species WHERE species_name = ?", (rs, rowNum) -> rs.getString("species_name"), treeSpecies.getSpeciesName()).isEmpty()){
                jdbcTemplate.update("INSERT INTO species_location_relationship (location_id, species_id) " +
                        "VALUES ((SELECT location_id FROM location WHERE country = ? LIMIT 1), " +
                        "(SELECT species_id FROM tree_species WHERE species_name = ? LIMIT 1))", location.getCountry(), treeSpecies.getSpeciesName());

                logger.debug("Relationship between " + location.getCountry() + " and " + treeSpecies.getSpeciesName() + " created successfully.");
            } else {
                logger.debug("Relationship can not be created.");
            }

            location.addSpecies(treeSpecies);
        });
        return location;
    }

    @Override
    public List<Locations> readLocations() {
        List<Locations> locations = jdbcTemplate.query("SELECT * FROM location ORDER BY location_id",
                (rs, rowNum) -> new Locations(rs.getInt("location_id"), rs.getString("country"), mapArrayToClimateArray(rs.getArray("sub_climates")),
                        rs.getDouble("yearly_average_rainfall"), rs.getInt("percentage_of_forestland")));

        // Load the relationships for location and treeSpecies/trees
        locations.forEach(location -> {
            location.addMultipleSpecies(jdbcTemplate.query("SELECT * FROM tree_species t JOIN species_location_relationship slr ON t.species_id = slr.species_id WHERE location_id = (SELECT location_id FROM location WHERE country = ? LIMIT 1)",
                    (rs, rowNum) -> new TreeSpecies(rs.getInt("species_id"), rs.getString("species_name"), LeafType.valueOf(rs.getString("leaf_type")),
                            ConservationStatus.valueOf(rs.getString("conservation_status")), rs.getBoolean("bears_fruit")), location.getCountry()));

            location.addMultipleTrees(jdbcTemplate.query("SELECT * FROM tree WHERE location_id = (SELECT location_id FROM location WHERE country = ? LIMIT 1)",
                    (rs, rowNum) -> new Tree(rs.getInt("tree_id"), rs.getDate("planting_date").toLocalDate(),
                            getTreeSpecies(rs.getInt("species_id")), location, rs.getDouble("height")), location.getCountry()));
        });

        locationsList = locations;

        logger.debug("All locations read");

        return locations;
    }

    @Override
    public Locations getLocationByName(String countryName){
        try {
            Locations location = readLocations().stream().filter(locations -> locations.getCountry().equalsIgnoreCase(countryName)).toList().get(0);
            logger.debug("Locations read by country");

            return location;
        } catch (ArrayIndexOutOfBoundsException e){
            logger.debug("Can't locate the country");
            throw new DataAccessException("No such country") {};
        }
    }

    @Override
    public List<Locations> findWhereMinForestCoverage(int forestCoverage){
        logger.debug("Querying database for all locations where forest coverage is above " + forestCoverage);
        return readLocations().stream().filter(locations -> locations.getPercentageOfForestland() >= forestCoverage).toList();
    }

    @Override
    public List<Locations> findWhereRainfallBelow(double rainfall){
        logger.debug("Querying database for all locations where rainfall is above " + rainfall);
        return readLocations().stream().filter(locations -> locations.getYearlyAverageRainfall() <= rainfall).toList();
    }

    @Override
    public List<Locations> findWhereNumberOfSpeciesAbove(int number){
        logger.debug("Querying database for all locations where there are more than " + number + " species");
        return readLocations().stream().filter(locations -> locations.getSpeciesFound().size() >= number).toList();
    }

    @Override
    public void removeLocation(String countryName) {
        jdbcTemplate.update("DELETE FROM species_location_relationship WHERE location_id = (SELECT location_id FROM location WHERE LOWER(country) = ? LIMIT 1)", countryName);
        jdbcTemplate.update("DELETE FROM tree WHERE location_id = (SELECT location_id FROM location WHERE LOWER(country) = ? LIMIT 1)" ,countryName);
        jdbcTemplate.update("DELETE FROM location WHERE LOWER(country) = ?", countryName);

        logger.debug(countryName + " removed successfully");
    }

    @Override
    public void updateLocation(Locations location) {
        logger.debug("Updating location with ID " + location.getLocationId());
        String[] subClimates = Arrays.stream(location.getSubClimates()).map(Enum::name).toArray(String[]::new);
        jdbcTemplate.update("UPDATE location SET country = ?, sub_climates = ?, yearly_average_rainfall = ?, percentage_of_forestland = ? WHERE location_id = ?",
                location.getCountry(), subClimates, location.getYearlyAverageRainfall(), location.getPercentageOfForestland(), location.getLocationId());
    }

    private Climate[] mapArrayToClimateArray(Array array) throws SQLException {
        Object[] climateObjects = (Object[]) array.getArray();
        String[] climateStrings = Arrays.copyOf(climateObjects, climateObjects.length, String[].class);
        Climate[] climates = new Climate[climateStrings.length];

        for (int i = 0; i < climateStrings.length; i++) {
            climates[i] = Climate.valueOf(climateStrings[i]);
        }

        return climates;
    }

    private TreeSpecies getTreeSpecies(int speciesID) {
        return jdbcTemplate.query("SELECT * FROM tree_species WHERE species_id = ?",
                (rs, rowNum) -> new TreeSpecies(rs.getString("species_name"), LeafType.valueOf(rs.getString("leaf_type")),
                        ConservationStatus.valueOf(rs.getString("conservation_status")), rs.getBoolean("bears_fruit")), speciesID).get(0);
    }
}
