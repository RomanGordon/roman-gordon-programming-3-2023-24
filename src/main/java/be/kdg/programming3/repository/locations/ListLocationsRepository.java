package be.kdg.programming3.repository.locations;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.TreeSpecies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

@Repository
@Profile("javacollections")
public class ListLocationsRepository implements LocationsRepository {
    private final Logger logger = LoggerFactory.getLogger(LocationsRepository.class);
    public static List<Locations> locationsList = new ArrayList<>();

    @Override
    public Locations createLocation(Locations location) {
        locationsList.add(location);

        logger.debug("Added new location to repository: " + location.getCountry());

        return location;
    }

    @Override
    public Locations addLocationSpecies(Locations location, List<TreeSpecies> species) {
        if (locationsList.contains(location)){
            int position = locationsList.indexOf(location);
            species.forEach(specie -> {
                locationsList.get(position).addSpecies(specie);
                specie.addLocation(location);
            });

            logger.debug("Added new tree species to existing location");

        } else {
            species.forEach(location::addSpecies);
            species.forEach(specie -> specie.addLocation(location));
            locationsList.add(location);

            logger.debug("Added new tree species to new location");
        }

        return location;
    }

    @Override
    public List<Locations> readLocations() {
        return locationsList;
    }

    @Override
    public Locations getLocationByName(String countryName) {
        return readLocations().stream().filter(location -> Objects.equals(location.getCountry().toLowerCase(), countryName.toLowerCase())).findFirst().orElse(null);
    }

    @Override
    public List<Locations> findWhereMinForestCoverage(int forestCoverage) {
        return readLocations().stream().filter(location -> location.getPercentageOfForestland() >= forestCoverage).toList();
    }

    @Override
    public List<Locations> findWhereRainfallBelow(double rainfall) {
        return readLocations().stream().filter(location -> location.getYearlyAverageRainfall() <= rainfall).toList();
    }

    @Override
    public List<Locations> findWhereNumberOfSpeciesAbove(int number) {
        return readLocations().stream().filter(location -> location.getSpeciesFound().size() >= number).toList();

    }

    @Override
    public void removeLocation(String countryName) {
        locationsList.removeIf(location -> Objects.equals(location.getCountry().toLowerCase(), countryName.toLowerCase()));
    }

    @Override
    public void updateLocation(Locations location) {
        locationsList.forEach(locations -> {
            if (locations.getLocationId() == location.getLocationId()) {
                locations.setCountry(location.getCountry());
                locations.setSubClimates(location.getSubClimates());
                locations.setYearlyAverageRainfall(location.getYearlyAverageRainfall());
                locations.setPercentageOfForestland(location.getPercentageOfForestland());
            }
        });
    }
}
