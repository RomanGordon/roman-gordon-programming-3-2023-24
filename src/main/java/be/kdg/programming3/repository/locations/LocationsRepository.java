package be.kdg.programming3.repository.locations;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.TreeSpecies;

import java.util.List;

public interface LocationsRepository {
    Locations createLocation(Locations location);

    Locations addLocationSpecies(Locations location, List<TreeSpecies> species);

    List<Locations> readLocations();

    Locations getLocationByName(String countryName);

    List<Locations> findWhereMinForestCoverage(int forestCoverage);

    List<Locations> findWhereRainfallBelow(double rainfall);

    List<Locations> findWhereNumberOfSpeciesAbove(int number);

    void removeLocation(String countryName);

    void updateLocation(Locations location);
}
