package be.kdg.programming3.repository;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.Tree;
import be.kdg.programming3.domain.TreeSpecies;
import be.kdg.programming3.repository.locations.LocationsRepository;
import be.kdg.programming3.repository.treeSpecies.TreeSpeciesRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

@Component
@Profile("javacollections")
public class DataFactory implements CommandLineRunner {
    private final LocationsRepository locationsRepository;
    private final TreeSpeciesRepository treeSpeciesRepository;

    public static List<Tree> treeList = new ArrayList<>();

    private static TreeSpecies currentSpecies;
    private static Locations currentLocation;

    public DataFactory(LocationsRepository locationsRepository, TreeSpeciesRepository treeSpeciesRepository) {
        this.locationsRepository = locationsRepository;
        this.treeSpeciesRepository = treeSpeciesRepository;
    }

    public void seed() {
        Random r = new Random();

        // Make trees
        TreeSpecies treeSpecies1 = new TreeSpecies(1, "Aspen", TreeSpecies.LeafType.LOBED, TreeSpecies.ConservationStatus.POSSIBLY_THREATENED, false);
        TreeSpecies treeSpecies2 = new TreeSpecies(2, "Alder", TreeSpecies.LeafType.TOOTHED, TreeSpecies.ConservationStatus.NOT_THREATENED, true);
        TreeSpecies treeSpecies3 = new TreeSpecies(3, "Fir", TreeSpecies.LeafType.CONIFER, TreeSpecies.ConservationStatus.THREATENED, true);
        TreeSpecies treeSpecies4 = new TreeSpecies(4, "Cherry", TreeSpecies.LeafType.TOOTHED, TreeSpecies.ConservationStatus.THREATENED, false);
        TreeSpecies treeSpecies5 = new TreeSpecies(5, "Chestnut", TreeSpecies.LeafType.ENTIRE, TreeSpecies.ConservationStatus.NOT_EVALUATED, true);
        TreeSpecies treeSpecies6 = new TreeSpecies(6, "Cypress", TreeSpecies.LeafType.CONIFER, TreeSpecies.ConservationStatus.POSSIBLY_THREATENED, true);
        TreeSpecies treeSpecies7 = new TreeSpecies(7, "Elm", TreeSpecies.LeafType.SCALE_LEAF, TreeSpecies.ConservationStatus.EXTINCT, false);
        TreeSpecies treeSpecies8 = new TreeSpecies(8, "Hazel", TreeSpecies.LeafType.BROADLEAF, TreeSpecies.ConservationStatus.THREATENED, false);
        TreeSpecies treeSpecies9 = new TreeSpecies(9, "Oak", TreeSpecies.LeafType.BROADLEAF, TreeSpecies.ConservationStatus.NOT_THREATENED, true);
        TreeSpecies treeSpecies10 = new TreeSpecies(10, "Maple", TreeSpecies.LeafType.SCALE_LEAF, TreeSpecies.ConservationStatus.THREATENED, true);

        List<TreeSpecies> treeSpecies = new ArrayList<>();
        treeSpecies.add(treeSpecies1);
        treeSpecies.add(treeSpecies2);
        treeSpecies.add(treeSpecies3);
        treeSpecies.add(treeSpecies4);
        treeSpecies.add(treeSpecies5);
        treeSpecies.add(treeSpecies6);
        treeSpecies.add(treeSpecies7);
        treeSpecies.add(treeSpecies8);
        treeSpecies.add(treeSpecies9);
        treeSpecies.add(treeSpecies10);

        // Make countries
        Locations locations1 = new Locations(1, "Belgium", new Locations.Climate[]{Locations.Climate.TEMPERATE, Locations.Climate.MARITIME}, 986.4, 23);
        Locations locations2 = new Locations(2, "Austria", new Locations.Climate[]{Locations.Climate.ALPINE, Locations.Climate.TEMPERATE}, 1090.8, 47);
        Locations locations3 = new Locations(3, "Switzerland", new Locations.Climate[]{Locations.Climate.TEMPERATE, Locations.Climate.ALPINE}, 1289.05, 32);
        Locations locations4 = new Locations(4, "Czechia", new Locations.Climate[]{Locations.Climate.TEMPERATE, Locations.Climate.TUNDRA}, 450.5, 35);
        Locations locations5 = new Locations(5, "Hungary", new Locations.Climate[]{Locations.Climate.CONTINENTAL}, 557, 23);
        Locations locations6 = new Locations(6, "Poland", new Locations.Climate[]{Locations.Climate.MARITIME, Locations.Climate.CONTINENTAL}, 643.21, 31);
        Locations locations7 = new Locations(7, "Portugal", new Locations.Climate[]{Locations.Climate.MARITIME, Locations.Climate.SUBTROPICAL}, 796.84, 36);
        Locations locations8 = new Locations(8, "Denmark", new Locations.Climate[]{Locations.Climate.TEMPERATE, Locations.Climate.ALPINE, Locations.Climate.POLAR}, 746, 16);
        Locations locations9 = new Locations(9, "France", new Locations.Climate[]{Locations.Climate.ALPINE, Locations.Climate.TUNDRA}, 836.54, 32);
        Locations locations10 = new Locations(10, "Liechtenstein", new Locations.Climate[]{Locations.Climate.CONTINENTAL, Locations.Climate.ALPINE}, 1000, 42);

        List<Locations> locationsList = new ArrayList<>();
        locationsList.add(locations1);
        locationsList.add(locations2);
        locationsList.add(locations3);
        locationsList.add(locations4);
        locationsList.add(locations5);
        locationsList.add(locations6);
        locationsList.add(locations7);
        locationsList.add(locations8);
        locationsList.add(locations9);
        locationsList.add(locations10);


        // Add countries to tree species
        treeSpecies.forEach(treeSpecie -> {
            if (treeSpecie.getConservationStatus() != TreeSpecies.ConservationStatus.EXTINCT) {
                int numberToAdd = r.nextInt(1, 3);
                List<Integer> countries = new ArrayList<>();

                while (countries.size() < numberToAdd) {
                    int number = r.nextInt(10);
                    if (!countries.contains(number)) {
                        countries.add(number);
                        treeSpecie.addLocation(locationsList.get(number));
                        locationsList.get(number).addSpecies(treeSpecie);
                    }
                }
            }
        });

        // Add tree species to countries and vice versa
        locationsList.forEach(location -> {
            int numberToAdd = r.nextInt(1,3);
            List<Integer> species = new ArrayList<>();

            while(species.size() < numberToAdd) {
                int number = r.nextInt(10);
                if (treeSpecies.get(number).getConservationStatus() != TreeSpecies.ConservationStatus.EXTINCT) {
                    if (!species.contains(number)) {
                        species.add(number);
                        location.addSpecies(treeSpecies.get(number));
                        treeSpecies.get(number).addLocation(location);
                    }
                }
            }
        });

        locationsList.forEach(location -> {
            currentLocation = location;
            location.getSpeciesFound().forEach(treeSpecie -> {
                currentSpecies = treeSpecie;
                if (treeSpecie.getConservationStatus() != TreeSpecies.ConservationStatus.EXTINCT) {
                    List<Tree> trees = Stream.generate(DataFactory::makeTree).limit(r.nextInt(3, 10)).toList();
                    trees.forEach(tree -> {
                        location.addMultipleTrees(trees);
                        treeSpecie.addMultipleTrees(trees);
                    });
                    treeList.addAll(trees);
                }
            });
        });

        treeSpecies.forEach(treeSpeciesRepository::createSpecies);
        locationsList.forEach(locationsRepository::createLocation);
    }

    public static Tree makeTree(){
        Random r = new Random();
        int id = r.nextInt(100000,999999);
        LocalDate plantingDate = LocalDate.of(r.nextInt(1950,2022), r.nextInt(1,12), r.nextInt(1,28));
        double height = r.nextDouble(0.5,5);

        return new Tree(id, plantingDate, currentSpecies, currentLocation, height);
    }


    @Override
    public void run(String... args) throws Exception {
        seed();
    }
}
