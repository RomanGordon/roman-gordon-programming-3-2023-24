package be.kdg.programming3.repository.treeSpecies;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.Locations.*;
import be.kdg.programming3.domain.Tree;
import be.kdg.programming3.domain.TreeSpecies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Using JDBC templates
@Repository
@Profile("jdbcdatabase")
public class JDBCTreeSpeciesRepository implements TreeSpeciesRepository{
    private final Logger logger = LoggerFactory.getLogger(TreeSpeciesRepository.class);
    public static List<TreeSpecies> treeSpeciesList = new ArrayList<>();
    JdbcTemplate jdbcTemplate;

    public JDBCTreeSpeciesRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public TreeSpecies createSpecies(TreeSpecies species) {
        jdbcTemplate.update("INSERT INTO tree_species (species_name, leaf_type, conservation_status, bears_fruit) VALUES (?, ?, ?, ?)",
                species.getSpeciesName(), species.getLeafType().toString(), species.getConservationStatus().toString(),
                species.bearsFruit());

        logger.debug("TreeSpecies " + species.getSpeciesName() + " created successfully");
        return species;
    }

    @Override
    public TreeSpecies addSpeciesLocations(TreeSpecies species, List<Locations> locations) {
        locations.forEach(location -> {
            if (jdbcTemplate.query("SELECT * FROM tree_species WHERE species_name = ?", (rs, rowNum) -> rs.getString("species_name"), species.getSpeciesName()).isEmpty()){
                createSpecies(species);
            }

            List<Integer> relationships = jdbcTemplate.query("SELECT * FROM species_location_relationship WHERE location_id = (SELECT location_id FROM location WHERE country = ? LIMIT 1)",
                    (rs, rowNum) -> rs.getInt("species_id") , location.getCountry());

            if (!relationships.contains(species.getSpeciesId()) &&
                    !jdbcTemplate.query("SELECT * FROM location WHERE country = ?", (rs, rowNum) -> rs.getString("country"), location.getCountry()).isEmpty()){
                jdbcTemplate.update("INSERT INTO species_location_relationship (location_id, species_id) " +
                                        "VALUES ((SELECT location_id FROM location WHERE country = ? LIMIT 1), " +
                                        "(SELECT species_id FROM tree_species WHERE species_name = ? LIMIT 1))", location.getCountry(), species.getSpeciesName());

                logger.debug("Relationship between " + species.getSpeciesName() + " and " + location.getCountry() + " created successfully.");
            } else {
                logger.debug("Relationship can not be created.");
            }

            species.addLocation(location);
        });
        return species;
    }

    @Override
    public List<TreeSpecies> readSpecies() {
        List<TreeSpecies> treeSpecies = jdbcTemplate.query("SELECT * FROM tree_species ORDER BY species_id",
                (rs, rowNum) -> new TreeSpecies(rs.getInt("species_id"), rs.getString("species_name"), TreeSpecies.LeafType.valueOf(rs.getString("leaf_type")),
                        TreeSpecies.ConservationStatus.valueOf(rs.getString("conservation_status")), rs.getBoolean("bears_fruit")));

        // Load the relationships for treeSpecies and locations/trees
        treeSpecies.forEach(species -> {
            species.addMultipleLocations(jdbcTemplate.query("SELECT * FROM location l JOIN species_location_relationship slr ON l.location_id = slr.location_id WHERE species_id = (SELECT species_id FROM tree_species WHERE species_name = ? LIMIT 1)",
                    (rs, rowNum) -> new Locations(rs.getInt("location_id"), rs.getString("country"), mapArrayToClimateArray(rs.getArray("sub_climates")),
                            rs.getDouble("yearly_average_rainfall"), rs.getInt("percentage_of_forestland")), species.getSpeciesName()));

            species.addMultipleTrees(jdbcTemplate.query("SELECT * FROM tree WHERE species_id = (SELECT species_id FROM tree_species WHERE species_name = ? LIMIT 1)",
                    (rs, rowNum) -> new Tree(rs.getInt("tree_id"), rs.getDate("planting_date").toLocalDate(),
                            species, getLocations(rs.getInt("location_id")), rs.getDouble("height")), species.getSpeciesName()));
        });

        treeSpeciesList = treeSpecies;

        logger.debug("Tree Species read successfully");

        return treeSpecies;
    }

    @Override
    public TreeSpecies getSpeciesByName(String speciesName){
        try {
            TreeSpecies species = readSpecies().stream().filter(specie -> specie.getSpeciesName().equalsIgnoreCase(speciesName)).toList().get(0);
            logger.debug("Species read successfully");

            return species;
        } catch (ArrayIndexOutOfBoundsException e){
            throw new DataAccessException("No such species") {};
        }
    }

    @Override
    public void removeSpecies(String speciesName) {
        jdbcTemplate.update("DELETE FROM species_location_relationship WHERE species_id = (SELECT species_id FROM tree_species WHERE LOWER(species_name) = ? LIMIT 1)", speciesName);
        jdbcTemplate.update("DELETE FROM tree WHERE species_id = (SELECT species_id FROM tree_species WHERE LOWER(species_name) = ? LIMIT 1)" ,speciesName);
        jdbcTemplate.update("DELETE FROM tree_species WHERE LOWER(species_name) = ?", speciesName);

        logger.debug("Tree species removed successfully");
    }

    @Override
    public void updateSpecies(TreeSpecies species) {
        logger.debug("Updating species with ID " + species.getSpeciesId());

        jdbcTemplate.update("UPDATE tree_species SET species_name = ?, leaf_type = ?, conservation_status = ?, bears_fruit = ? WHERE species_id = ?",
                species.getSpeciesName(), species.getLeafType().toString(), species.getConservationStatus().toString(),
                species.bearsFruit(), species.getSpeciesId());
    }

    private Locations getLocations(int country) {
        return jdbcTemplate.query("SELECT * FROM location WHERE location_id = ?",
                (rs, rowNum) -> new Locations(rs.getString("country"), mapArrayToClimateArray(rs.getArray("sub_climates")),
                        rs.getDouble("yearly_average_rainfall"), rs.getInt("percentage_of_forestland")), country).get(0);
    }

    private Climate[] mapArrayToClimateArray(Array array) throws SQLException {
        Object[] climateObjects = (Object[]) array.getArray();
        String[] climateStrings = Arrays.copyOf(climateObjects, climateObjects.length, String[].class);
        Climate[] climates = new Climate[climateStrings.length];

        for (int i = 0; i < climateStrings.length; i++) {
            climates[i] = Climate.valueOf(climateStrings[i]);
        }

        return climates;
    }
}
