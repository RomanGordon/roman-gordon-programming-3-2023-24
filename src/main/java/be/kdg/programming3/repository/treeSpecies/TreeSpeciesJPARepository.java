package be.kdg.programming3.repository.treeSpecies;

import be.kdg.programming3.domain.TreeSpecies;
import be.kdg.programming3.domain.TreeSpecies.*;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Profile("jparepository")
public interface TreeSpeciesJPARepository extends JpaRepository<TreeSpecies, Integer> {
    List<TreeSpecies> findAllByConservationStatusEquals(ConservationStatus conservationStatus);
    List<TreeSpecies> findBySpeciesNameEqualsIgnoreCase(String speciesName);
    void deleteBySpeciesNameEqualsIgnoreCase(String speciesName);
}
