package be.kdg.programming3.repository.treeSpecies;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.TreeSpecies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
@Profile("javacollections")
public class ListTreeSpeciesRepository implements TreeSpeciesRepository {
    private final Logger logger = LoggerFactory.getLogger(TreeSpeciesRepository.class);
    public static List<TreeSpecies> treeSpeciesList = new ArrayList<>();

    @Override
    public TreeSpecies createSpecies(TreeSpecies species){
        treeSpeciesList.add(species);

        logger.debug("Added new species to repository: " + species.getSpeciesName());

        return species;
    }

    @Override
    public TreeSpecies addSpeciesLocations(TreeSpecies species, List<Locations> locations) {
        if (treeSpeciesList.contains(species)){
            int position = treeSpeciesList.indexOf(species);
            locations.forEach(location -> {
                treeSpeciesList.get(position).addLocation(location);
                location.addSpecies(species);
            });

            logger.debug("Added new locations to existing tree species");

        } else {
            locations.forEach(species::addLocation);
            locations.forEach(location -> location.addSpecies(species));
            treeSpeciesList.add(species);

            logger.debug("Added new locations to new tree species");
        }

        return species;
    }

    @Override
    public List<TreeSpecies> readSpecies() {
        return treeSpeciesList;
    }

    @Override
    public TreeSpecies getSpeciesByName(String speciesName) {
        return readSpecies().stream().filter(species -> Objects.equals(species.getSpeciesName().toLowerCase(), speciesName.toLowerCase())).findFirst().orElse(null);
    }

    @Override
    public void removeSpecies(String speciesName) {
        treeSpeciesList.removeIf(species -> Objects.equals(species.getSpeciesName().toLowerCase(), speciesName.toLowerCase()));
    }

    @Override
    public void updateSpecies(TreeSpecies species) {
        treeSpeciesList.forEach(treeSpecies -> {
            if (treeSpecies.getSpeciesId() == species.getSpeciesId()){
                treeSpecies.setSpeciesName(species.getSpeciesName());
                treeSpecies.setLeafType(species.getLeafType());
                treeSpecies.setConservationStatus(species.getConservationStatus());
                treeSpecies.setBearsFruit(species.bearsFruit());
            }
        });
    }
}
