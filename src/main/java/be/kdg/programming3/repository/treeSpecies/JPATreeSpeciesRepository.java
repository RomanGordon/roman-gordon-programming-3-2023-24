package be.kdg.programming3.repository.treeSpecies;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.TreeSpecies;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

import java.util.List;

@Repository
@Profile("jpadatabase")
public class JPATreeSpeciesRepository implements TreeSpeciesRepository{
    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public TreeSpecies createSpecies(TreeSpecies species) {
        em.persist(species);
        return species;
    }

    @Override
    public TreeSpecies addSpeciesLocations(TreeSpecies species, List<Locations> locations) {
        locations.forEach(species::addLocation);
        return species;
    }

    @Override
    public List<TreeSpecies> readSpecies() {
        return em.createQuery("SELECT species FROM TreeSpecies species", TreeSpecies.class).getResultList();
    }

    @Override
    @Transactional
    public TreeSpecies getSpeciesByName(String speciesName){
        Query query = em.createQuery("SELECT species FROM TreeSpecies species WHERE LOWER(species.speciesName) = ?1", Locations.class);
        query.setParameter(1, speciesName.toLowerCase());
        return (TreeSpecies) query.getSingleResult();
    }

    @Override
    @Transactional
    public void removeSpecies(String speciesName) {
        Query query = em.createQuery("SELECT species FROM TreeSpecies species WHERE LOWER(species.speciesName) = ?1", TreeSpecies.class);
        query.setParameter(1, speciesName.toLowerCase());
        TreeSpecies species = (TreeSpecies) query.getSingleResult();
        em.remove(species);
    }

    @Override
    public void updateSpecies(TreeSpecies species) {
        em.merge(species);
    }
}
