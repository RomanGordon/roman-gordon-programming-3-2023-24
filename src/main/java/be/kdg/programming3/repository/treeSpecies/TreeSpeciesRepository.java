package be.kdg.programming3.repository.treeSpecies;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.TreeSpecies;
import jakarta.transaction.Transactional;

import java.util.List;

public interface TreeSpeciesRepository {
    TreeSpecies createSpecies(TreeSpecies species);

    TreeSpecies addSpeciesLocations(TreeSpecies species, List<Locations> locations);

    List<TreeSpecies> readSpecies();

    @Transactional
    TreeSpecies getSpeciesByName(String speciesName);

    void removeSpecies(String speciesName);

    void updateSpecies(TreeSpecies species);
}
