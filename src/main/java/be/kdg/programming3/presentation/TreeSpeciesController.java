package be.kdg.programming3.presentation;

import be.kdg.programming3.domain.TreeSpecies;
import be.kdg.programming3.presentation.viewmodels.AddSpeciesViewModel;
import be.kdg.programming3.service.treeSpecies.TreeSpeciesService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/species")
public class TreeSpeciesController {
    private final Logger logger = LoggerFactory.getLogger(TreeSpeciesController.class);
    private final TreeSpeciesService treeSpeciesService;

    public TreeSpeciesController(TreeSpeciesService treeSpeciesService) {
        this.treeSpeciesService = treeSpeciesService;
    }

    @GetMapping
    public String getSpecies(Model model, HttpSession session) {
        PagesViewedController.addPageVisited("All TreeSpecies", session);

        logger.info("Request for all tree species in view");
        List<TreeSpecies> treeSpeciesList = treeSpeciesService.getSpecies();
        model.addAttribute("treeSpecies", treeSpeciesList);
        model.addAttribute("active_AllSpecies", "active");
        return "allTreeSpecies";
    }

    @GetMapping("/add")
    public String addSpecies(Model model, HttpSession session){
        PagesViewedController.addPageVisited("Add TreeSpecies", session);

        logger.info("View to add tree species");

        addAttributesForAddPageToModel(model);

        model.addAttribute("speciesViewModel", new AddSpeciesViewModel());

        return "addTreeSpecies";
    }

    @PostMapping("/add")
    public String processAddSpecies(@Valid @ModelAttribute("speciesViewModel") AddSpeciesViewModel speciesViewModel, BindingResult result, Model model){
        logger.info("Adding a Tree Species");

        if (result.hasErrors()){
            result.getAllErrors().forEach(e->logger.warn(e.toString()));

            addAttributesForAddPageToModel(model);

            return "addTreeSpecies";
        } else {
            try {
                TreeSpecies species = treeSpeciesService.getSpeciesByName(speciesViewModel.getSpeciesName());
                if (species == null) throw new DataAccessException ("Tree Species does not exist"){};
                model.addAttribute("speciesExists", "A species with this name already exists!");
                addAttributesForAddPageToModel(model);
                logger.debug("Tree species already exists!");

                return "addTreeSpecies";
            } catch (DataAccessException e){
                logger.info("Tree species does not exist, continuing to add");
            }

            treeSpeciesService.addSpecies(speciesViewModel.getSpeciesName(),
                                          speciesViewModel.getLeaf(),
                                          speciesViewModel.getStatus(),
                                          speciesViewModel.getBearsFruit());
            return "redirect:/species";
        }
    }

    private void addAttributesForAddPageToModel(Model model){
        model.addAttribute("leafTypes", Arrays.stream(TreeSpecies.LeafType.values()).toList());
        model.addAttribute("conservationStats", Arrays.stream(TreeSpecies.ConservationStatus.values()).toList());
        model.addAttribute("active_AddSpecies", "active");
        model.addAttribute("headerText", "Add");
    }

    @GetMapping("/{species}")
    public String showDetailsPage(@PathVariable String species, Model model, HttpSession session) {
        logger.info("Showing a more detailed page for " + species);
        TreeSpecies treeSpecies = treeSpeciesService.getSpeciesByName(species);
        model.addAttribute("species", treeSpecies);

        PagesViewedController.addPageVisited(species + " species page", session);
        return "speciesInfo";
    }

    @GetMapping("/del/{species}")
    public String deleteSpecies(@PathVariable String species, Model model){
        logger.debug("Deleting a species");
        treeSpeciesService.removeSpecies(species);

        return "redirect:/species";
    }

    @GetMapping("/edit/{species}")
    public String editSpecies(@PathVariable String species, Model model, HttpSession session){
        logger.debug("Going to edit " + species);

        TreeSpecies treeSpecies = treeSpeciesService.getSpeciesByName(species);

        model.addAttribute("species", treeSpecies);
        model.addAttribute("headerText", "Edit");

        model.addAttribute("leafTypes", Arrays.stream(TreeSpecies.LeafType.values()).toList());
        model.addAttribute("conservationStats", Arrays.stream(TreeSpecies.ConservationStatus.values()).toList());

        model.addAttribute("speciesViewModel", new AddSpeciesViewModel(
                treeSpecies.getSpeciesName(), treeSpecies.getConservationStatus(), treeSpecies.getLeafType(), treeSpecies.bearsFruit()));

        PagesViewedController.addPageVisited("Edit " + species + " page", session);

        return "addTreeSpecies";
    }

    @PostMapping("/edit/{species}")
    public String processEditSpecies(@PathVariable String species, @Valid @ModelAttribute("speciesViewModel") AddSpeciesViewModel speciesViewModel, BindingResult result, Model model){

        if (result.hasErrors()){
            result.getAllErrors().forEach(e->logger.warn(e.toString()));

            model.addAttribute("leafTypes", Arrays.stream(TreeSpecies.LeafType.values()).toList());
            model.addAttribute("conservationStats", Arrays.stream(TreeSpecies.ConservationStatus.values()).toList());

            TreeSpecies treeSpecies = treeSpeciesService.getSpeciesByName(species);
            model.addAttribute("species", treeSpecies);
            model.addAttribute("headerText", "Edit");

            return "addTreeSpecies";
        } else {
            TreeSpecies treeSpecies = treeSpeciesService.getSpeciesByName(species);

            treeSpeciesService.updateSpecies(treeSpecies.getSpeciesId(),
                                            speciesViewModel.getSpeciesName(),
                                            speciesViewModel.getLeaf(),
                                            speciesViewModel.getStatus(),
                                            speciesViewModel.getBearsFruit());

            return "redirect:/species";
        }
    }
}
