package be.kdg.programming3.presentation.handlers;

import org.hibernate.exception.SQLGrammarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.sql.SQLException;

@ControllerAdvice
public class GlobalExceptionHandler {
    private final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    // Redirect all database exceptions to the databaseError page
    @ExceptionHandler({DataAccessException.class, SQLException.class, SQLGrammarException.class})
    public String dataAccessException() {
        logger.error("A database error has occured");
        return "databaseError";
    }
}
