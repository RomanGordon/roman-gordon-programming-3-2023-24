package be.kdg.programming3.presentation.converters;

import org.springframework.core.convert.converter.Converter;

public class StringToEnumGenericConverter <T extends Enum<T>> implements Converter<String, T> {
    private final Class<T> targetType;

    public StringToEnumGenericConverter(Class<T> targetType) {
        this.targetType = targetType;
    }

    @Override
    public T convert(String source) {
        return Enum.valueOf(targetType, source);
    }
}
