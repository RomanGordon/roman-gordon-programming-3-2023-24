package be.kdg.programming3.presentation.converters;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.Locations.*;
import org.springframework.core.convert.converter.Converter;

import java.util.Arrays;
import java.util.List;

public class ListStringToArrayClimateConverter implements Converter<List<String>, Climate[]> {

    @Override
    public Climate[] convert(List<String> climates) {
        List<Locations.Climate> climateList = Arrays.stream(Locations.Climate.values()).toList();
        List<Locations.Climate> usedClimates = climateList.stream()
                .filter(climateListItem -> climates.contains(climateListItem.toString()))
                .toList();

        return usedClimates.toArray(new Locations.Climate[0]);
    }
}
