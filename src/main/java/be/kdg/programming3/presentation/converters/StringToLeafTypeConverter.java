package be.kdg.programming3.presentation.converters;

import be.kdg.programming3.domain.TreeSpecies.*;
import org.springframework.core.convert.converter.Converter;

// USING STRING TO ENUM GENERIC CONVERTER INSTEAD
public class StringToLeafTypeConverter implements Converter<String, LeafType> {
    @Override
    public LeafType convert(String leaf) {
        return LeafType.valueOf(leaf);
    }
}
