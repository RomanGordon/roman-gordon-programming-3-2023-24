package be.kdg.programming3.presentation.converters;

import be.kdg.programming3.domain.TreeSpecies.*;
import org.springframework.core.convert.converter.Converter;

// USING STRING TO ENUM GENERIC CONVERTER INSTEAD
public class StringToConservationStatusConverter implements Converter<String, ConservationStatus> {
    @Override
    public ConservationStatus convert(String status) {
        return ConservationStatus.valueOf(status);
    }
}
