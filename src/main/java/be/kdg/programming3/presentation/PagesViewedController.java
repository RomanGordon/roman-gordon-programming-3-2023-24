package be.kdg.programming3.presentation;

import jakarta.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@RequestMapping("/viewed")
public class PagesViewedController {
    private static final Logger logger = LoggerFactory.getLogger(PagesViewedController.class);

    @GetMapping
    public String getPagesViewed(Model model, HttpSession session) {
        addPageVisited("Session History", session);

        logger.info("Request for all pages viewed");
        model.addAttribute("active_PagesViewed", "active");
        return "allPagesViewed";
    }

    // Add a page visited to the session, add into the map of Timestamp, String
    public static void addPageVisited(String page, HttpSession session){
        Map<Timestamp, String> pagesVisited = (Map<Timestamp, String>) session.getAttribute("pagesVisited");

        if (pagesVisited == null){
            logger.info("Created new page list");
            pagesVisited = new LinkedHashMap<>();

            session.setAttribute("pagesVisited", pagesVisited);
        }

        pagesVisited.put(Timestamp.from(Instant.now()), page);

        logger.debug("Added " + page + " to pages visited");

    }
}
