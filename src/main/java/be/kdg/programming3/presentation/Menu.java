package be.kdg.programming3.presentation;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.TreeSpecies;
import be.kdg.programming3.domain.TreeSpecies.ConservationStatus;
import be.kdg.programming3.service.locations.LocationsService;
import be.kdg.programming3.service.treeSpecies.TreeSpeciesService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

// This is a console version of the application with limited functionality
// run by calling show method in application run class
// does not work with JPARepository, jpa database or jdbc implementations
@Component
public class Menu {
    private final TreeSpeciesService treeSpeciesService;
    private final LocationsService locationsService;

    public Menu(TreeSpeciesService treeSpeciesService, LocationsService locationsService) {
        this.treeSpeciesService = treeSpeciesService;
        this.locationsService = locationsService;
    }

    private final Scanner scanner = new Scanner(System.in);
    public void show() {
        boolean quit = false;

        while (!quit) {
            int choice;
            System.out.println("""
                    
                    What would you like to do?
                    ==========================
                    0) Quit
                    1) Show all Tree Species
                    2) Show tree species of conservation status
                    3) Show all locations
                    4) Show locations with forest coverage and/or rainfall""");
            System.out.print("Choice (0-4): ");

            try {
                choice = scanner.nextInt();
            } catch (InputMismatchException e){
                choice = 6;
                scanner.nextLine();
            }

            switch (choice) {
                case 0 -> quit = true;
                case 1 -> showAllTreeSpecies();
                case 2 -> showFilteredTreeSpecies();
                case 3 -> showAllLocations();
                case 4 -> showFilteredLocations();
                default -> System.out.println("Invalid option...\n");
            }
        }
    }

    private void showAllTreeSpecies(){
        System.out.println("\nALL TREE SPECIES\n================");
        treeSpeciesService.getSpecies().forEach(System.out::println);

        treeSpeciesService.writeTreeSpeciesToJson(treeSpeciesService.getSpecies());
    }

    private void showFilteredTreeSpecies(){
        scanner.nextLine();

        ConservationStatus[] values = ConservationStatus.values();
        String statuses = IntStream.range(0, values.length)
                .mapToObj(i -> (i + 1) + "=" + values[i].getStatusString())
                .collect(Collectors.joining(", "));

        System.out.print("\nWhat conservation status? (" + statuses + ") ");
        try {
            int status = scanner.nextInt();

            List<TreeSpecies> filteredSpecies = new ArrayList<>();


            switch (status){
                case 1: case 2: case 3: case 4: case 5: filteredSpecies = treeSpeciesService.getFilteredSpecies(status); break;
                default: System.out.println("Invalid input...\n"); break;
            }

            if (filteredSpecies.isEmpty()){
                System.out.println("There are no trees of this status.");
            } else {
                System.out.println("\nFILTERED TREE SPECIES\n=====================");
                filteredSpecies.forEach(System.out::println);

                treeSpeciesService.writeTreeSpeciesToJson(filteredSpecies);
            }
        } catch (InputMismatchException e){
            System.out.println("Invalid conservation status choice!");
            scanner.nextLine();
        }


    }

    private void showAllLocations(){
        System.out.println("\nALL LOCATIONS\n=============");
        locationsService.getLocations().forEach(System.out::println);

        locationsService.writeLocationsToJson(locationsService.getLocations());
    }

    private void showFilteredLocations(){
        scanner.nextLine();

        System.out.print("Enter the minimum yearly rainfall to search for (double, optional): ");
        String minRainfall = scanner.nextLine();

        System.out.print("Enter the minimum % of forest coverage to search for (int, optional): ");
        String minForest = scanner.nextLine();

        List<Locations> filteredLocations = locationsService.getFilteredLocations(minForest, minRainfall);

        System.out.println("\nFILTERED LOCATIONS\n==================");
        filteredLocations.forEach(System.out::println);

        locationsService.writeLocationsToJson(filteredLocations);
    }
}
