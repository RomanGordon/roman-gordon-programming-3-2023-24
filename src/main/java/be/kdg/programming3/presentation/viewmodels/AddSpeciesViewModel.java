package be.kdg.programming3.presentation.viewmodels;

import be.kdg.programming3.domain.TreeSpecies.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public class AddSpeciesViewModel {
    @NotNull
    @Size(min=2, max=30, message = "Your species name must be between 2 and 30 characters!")
    @Pattern(regexp = "^[A-Za-z ]*$", message = "Your species name must not contain numbers!")
    private String speciesName;

    @NotNull(message = "You must select a conservation status!")
    private ConservationStatus status;

    @NotNull(message = "You must select a leaf type!")
    private LeafType leaf;

    @NotNull(message = "Does it bear fruit?")
    private Boolean bearsFruit;

    public AddSpeciesViewModel(String speciesName, ConservationStatus status, LeafType leaf, Boolean bearsFruit) {
        this.speciesName = speciesName;
        this.status = status;
        this.leaf = leaf;
        this.bearsFruit = bearsFruit;
    }

    public AddSpeciesViewModel() {
    }

    public String getSpeciesName() {
        return speciesName;
    }

    public void setSpeciesName(String speciesName) {
        this.speciesName = speciesName;
    }

    public ConservationStatus getStatus() {
        return status;
    }

    public void setStatus(ConservationStatus status) {
        this.status = status;
    }

    public LeafType getLeaf() {
        return leaf;
    }

    public void setLeaf(LeafType leaf) {
        this.leaf = leaf;
    }

    public Boolean getBearsFruit() {
        return bearsFruit;
    }

    public void setBearsFruit(Boolean bearsFruit) {
        this.bearsFruit = bearsFruit;
    }

    @Override
    public String toString() {
        return "AddSpeciesViewModel{" +
                "speciesName='" + speciesName + '\'' +
                ", status=" + status +
                ", leaf=" + leaf +
                ", bearsFruit=" + bearsFruit +
                '}';
    }
}
