package be.kdg.programming3.presentation.viewmodels;

import be.kdg.programming3.domain.Locations.*;
import be.kdg.programming3.domain.Tree;
import be.kdg.programming3.domain.TreeSpecies;
import jakarta.validation.constraints.*;

import java.util.Arrays;
import java.util.List;

public class AddLocationViewModel {
    @NotNull
    @Size(min=4, max=30, message = "Your country name must be between 4 and 30 characters!")
    @Pattern(regexp = "^[A-Za-z ]*$", message = "Your country name must not contain numbers!")
    private String countryName;
    @NotEmpty(message = "You must select at least 1 climate!")
    private Climate[] climates;
    @NotNull(message = "You must enter a rainfall amount!")
    @PositiveOrZero(message = "Rainfall must be 0mm or higher!")
    private Double rainfall;
    @NotNull(message = "You must add forest coverage!")
    @PositiveOrZero(message = "Forest Coverage must be 0% or higher!")
    @Max(value = 100, message = "Forest coverage can not be greater than 100%!")
    private Integer forestCoverage;

    public AddLocationViewModel(String countryName, Climate[] climates, Double rainfall, Integer forestCoverage) {
        this.countryName = countryName;
        this.climates = climates;
        this.rainfall = rainfall;
        this.forestCoverage = forestCoverage;
    }

    public AddLocationViewModel() {
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Climate[] getClimates() {
        return climates;
    }

    public void setClimates(Climate[] climates) {
        this.climates = climates;
    }

    public Double getRainfall() {
        return rainfall;
    }

    public void setRainfall(Double rainfall) {
        this.rainfall = rainfall;
    }

    public Integer getForestCoverage() {
        return forestCoverage;
    }

    public void setForestCoverage(Integer forestCoverage) {
        this.forestCoverage = forestCoverage;
    }

    @Override
    public String toString() {
        return "AddLocationViewModel{" +
                ", countryName='" + countryName + '\'' +
                ", climates=" + Arrays.toString(climates) +
                ", rainfall=" + rainfall +
                ", forestCoverage=" + forestCoverage +
                '}';
    }
}
