package be.kdg.programming3.presentation;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.presentation.viewmodels.AddLocationViewModel;
import be.kdg.programming3.service.locations.LocationsService;
import be.kdg.programming3.util.exception.InvalidValueException;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("/locations")
public class LocationsController {
    private final Logger logger = LoggerFactory.getLogger(LocationsController.class);
    private final LocationsService locationsService;

    public LocationsController(LocationsService locationsService) {
        this.locationsService = locationsService;
    }

    @GetMapping
    public String getLocations(Model model, HttpSession session) {
        PagesViewedController.addPageVisited("All Locations", session);

        logger.info("Request for all locations in view");
        List<Locations> locationsList = locationsService.getLocations();
        model.addAttribute("locations", locationsList);
        model.addAttribute("active_AllLocations", "active");
        model.addAttribute("value", "");
        model.addAttribute("forestSelected", "");
        return "allLocations"; // return the logical name of a view
    }

    @PostMapping
    public String filterLocations(String filterOption, String value, Model model){
        List<Locations> locationsList;
        model.addAttribute("active_AllLocations", "active");
        model.addAttribute("value", value);
        try {
            if (Objects.equals(filterOption, "forest")) {
                logger.info("Filtering by minimum forest coverage");
                model.addAttribute("forestSelected", "");
                locationsList = locationsService.findWhereMinForestCoverage(value);
            } else if (Objects.equals(filterOption, "species")) {
                logger.info("Filtering by number of species higher than");
                model.addAttribute("speciesSelected", "");
                locationsList = locationsService.findWhereNumberOfSpeciesAbove(value);
            } else if (Objects.equals(filterOption, "rainfall")) {
                logger.info("Filtering by minimum rainfall");
                model.addAttribute("rainSelected", "");
                locationsList = locationsService.findWhereRainfallBelow(value);
            } else {
                return "redirect:/locations";
            }
        } catch (InvalidValueException e){
            logger.warn("Incorrect input value!");
            locationsList = locationsService.getLocations();
            model.addAttribute("locations", locationsList);
            model.addAttribute("error", e.getMessage());
            return "allLocations";
        }
        model.addAttribute("locations", locationsList);
        return "allLocations";
    }

    @GetMapping("/add")
    public String addLocations(Model model, HttpSession session){
        PagesViewedController.addPageVisited("Add Locations", session);

        logger.info("View to add locations");
        addAttributesForAddPageToModel(model);

        model.addAttribute("locationViewModel", new AddLocationViewModel());
        return "addLocations";
    }

    @PostMapping("/add")
    public String processAddLocations(@Valid @ModelAttribute("locationViewModel") AddLocationViewModel locationViewModel, BindingResult result, Model model){
        logger.info("Adding a location");

        if (result.hasErrors()){
            result.getAllErrors().forEach(e->logger.warn(e.toString()));

            addAttributesForAddPageToModel(model);

            return "addLocations";
        } else {
            try {
                Locations location = locationsService.getLocationByName(locationViewModel.getCountryName());
                System.out.println(location == null);
                if (location == null) throw new DataAccessException ("Tree Species does not exist"){};
                model.addAttribute("countryExists", "A country with this name already exists!");
                addAttributesForAddPageToModel(model);
                logger.debug("Location already exists!");

                return "addLocations";
            } catch (DataAccessException e) {
                logger.info("Location does not exist, continuing to add location");
            }

            locationsService.addLocation(locationViewModel.getCountryName(),
                                         locationViewModel.getClimates(),
                                         locationViewModel.getRainfall(),
                                         locationViewModel.getForestCoverage());
            return "redirect:/locations";
        }
    }

    private void addAttributesForAddPageToModel(Model model){
        List<Locations.Climate> climateList = Arrays.stream(Locations.Climate.values()).toList();
        model.addAttribute("climates", climateList);
        model.addAttribute("active_AddLocations", "active");
        model.addAttribute("headerText", "Add");
    }

    @GetMapping("/{country}")
    public String showDetailsPage(@PathVariable String country, Model model, HttpSession session) {
        logger.info("Showing a more detailed page for " + country);
        Locations location = locationsService.getLocationByName(country);
        model.addAttribute("location", location);

        PagesViewedController.addPageVisited(country + " country page", session);
        return "locationInfo";
    }

    @GetMapping("/del/{country}")
    public String deleteLocation(@PathVariable String country, Model model){
        logger.debug("Deleting " + country);
        locationsService.removeLocation(country);

        return "redirect:/locations";
    }

    @GetMapping("/edit/{country}")
    public String editLocation(@PathVariable String country, Model model, HttpSession session){
        logger.debug("Going to edit " + country);

        Locations location = locationsService.getLocationByName(country);

        model.addAttribute("location", location);
        model.addAttribute("headerText", "Edit");


        List<Locations.Climate> climateList = Arrays.stream(Locations.Climate.values()).toList();
        model.addAttribute("climates", climateList);

        model.addAttribute("locationViewModel", new AddLocationViewModel(
                location.getCountry(), location.getSubClimates(), location.getYearlyAverageRainfall(),
                location.getPercentageOfForestland()));

        PagesViewedController.addPageVisited("Edit " + country + " page", session);

        return "addLocations";
    }

    @PostMapping("/edit/{country}")
    public String updateLocation(@PathVariable String country, @Valid @ModelAttribute("locationViewModel") AddLocationViewModel locationViewModel, BindingResult result, Model model) {
        logger.debug("Attempting to update " + country);

        if (result.hasErrors()){
            result.getAllErrors().forEach(e->logger.warn(e.toString()));

            List<Locations.Climate> climateList = Arrays.stream(Locations.Climate.values()).toList();
            model.addAttribute("climates", climateList);

            Locations location = locationsService.getLocationByName(country);
            model.addAttribute("location", location);
            model.addAttribute("headerText", "Edit");

            return "addLocations";
        } else {
            Locations location = locationsService.getLocationByName(country);

            locationsService.updateLocation(location.getLocationId(),
                    locationViewModel.getCountryName(),
                    locationViewModel.getClimates(),
                    locationViewModel.getRainfall(),
                    locationViewModel.getForestCoverage());

            return "redirect:/locations";
        }
    }
}
