package be.kdg.programming3;

import be.kdg.programming3.presentation.Menu;
import jakarta.servlet.http.HttpSession;
import org.apache.catalina.session.StandardSession;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Locale;

@SpringBootApplication
public class Programming3ProjectSpringApplication {

	public static void main(String[] args) {
		var context = SpringApplication.run(Programming3ProjectSpringApplication.class, args);
//		context.getBean(Menu.class).show();
//		context.close();

		//NOTE: In order to use console application you must use javacollections profile, uncomment the code above
		// and add transient to the list of tree species and to the list of trees in locations,
		// and then also to the list of locations and the list of trees in tree species
	}

}
