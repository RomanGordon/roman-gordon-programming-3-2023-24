package be.kdg.programming3.domain;

import jakarta.persistence.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tree_species")
public class TreeSpecies {
    @Transient
    private final Logger logger = LoggerFactory.getLogger(TreeSpecies.class);

    // ---------------------------------- //

    // Used from Locations
    @ManyToMany(cascade = {CascadeType. DETACH, CascadeType. MERGE,
            CascadeType.PERSIST, CascadeType. REFRESH}, fetch = FetchType.EAGER)
    @JoinTable(name="species_location_relationship",
            joinColumns = @JoinColumn(name = "species_id"),
            inverseJoinColumns = @JoinColumn(name="location_id"))
    private List<Locations> locationsFoundIn;
    // Add transient to list definition not to work with console application

    // ---------------------------------- //

    // Used from Tree
    @OneToMany(mappedBy = "species", cascade = CascadeType.ALL)
    private List<Tree> treesInSpecies;
    // Add transient to list definition not to work with console application

    public TreeSpecies() {}

    // ---------------------------------- //

    public enum LeafType {
        CONIFER("Conifer"), BROADLEAF("Broadleaf"), SCALE_LEAF("Scale Leaf"),
        LOBED("Lobed"), TOOTHED("Toothed"), ENTIRE("Entire");

        public final String leafType;

        LeafType(String label) {
            this.leafType = label;
        }

        public String getTypeString() {
            return leafType;
        }
    } // First 3 are evergreen, next 3 are deciduous

    public enum ConservationStatus {
        EXTINCT("Extinct"), THREATENED("Threatened"), POSSIBLY_THREATENED("Possibly Threatened"), NOT_THREATENED("Not Threatened"), NOT_EVALUATED("Not Evaluated");

        public final String status;

        ConservationStatus(String label) {
            this.status = label;
        }

        public String getStatusString() {
            return status;
        }
    }

    @Column(name = "species_name")
    private String speciesName;
    @Column(name = "leaf_type")
    @Enumerated(EnumType.STRING)
    private LeafType leafType;
    @Column(name = "conservation_status")
    @Enumerated(EnumType.STRING)
    private ConservationStatus conservationStatus;
    @Column(name = "bears_fruit")
    private boolean bearsFruit;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "species_id")
    private int speciesId;

    // ------------CONSTRUCTOR----------- //

    public TreeSpecies(String speciesName, LeafType leafType, ConservationStatus conservationStatus, List<Locations> locationsFoundIn, boolean bearsFruit) {
        this.speciesName = speciesName;
        this.leafType = leafType;
        this.conservationStatus = conservationStatus;
        this.locationsFoundIn = locationsFoundIn;
        this.bearsFruit = bearsFruit;
        this.treesInSpecies = new ArrayList<>();

//        logger.debug("Created TreeSpecies with name: " + speciesName + "; leaf type: " + leafType + "; conservation status: " + conservationStatus + "; locations found in: " + locationsFoundIn + "; bears fruit: " + bearsFruit);
    }

    public TreeSpecies(String speciesName, LeafType leafType, ConservationStatus conservationStatus, boolean bearsFruit) {
        this.speciesName = speciesName;
        this.leafType = leafType;
        this.conservationStatus = conservationStatus;
        this.bearsFruit = bearsFruit;
        this.locationsFoundIn = new ArrayList<>();
        this.treesInSpecies = new ArrayList<>();

//        logger.debug("Created TreeSpecies with name: " + speciesName + "; leaf type: " + leafType + "; conservation status: " + conservationStatus + "; bears fruit: " + bearsFruit);
    }

    public TreeSpecies(int speciesId, String speciesName, LeafType leafType, ConservationStatus conservationStatus, boolean bearsFruit) {
        this.speciesId = speciesId;
        this.speciesName = speciesName;
        this.leafType = leafType;
        this.conservationStatus = conservationStatus;
        this.bearsFruit = bearsFruit;
        this.locationsFoundIn = new ArrayList<>();
        this.treesInSpecies = new ArrayList<>();

//        logger.debug("Created TreeSpecies with name: " + speciesName + "; leaf type: " + leafType + "; conservation status: " + conservationStatus + "; bears fruit: " + bearsFruit);
    }
    // ---------------------------------- //

    // Adds 1 location to the tree species
    public void addLocation(Locations location){
        if (!(this.conservationStatus == ConservationStatus.EXTINCT)) {
            if (!this.locationsFoundIn.contains(location)) {
                this.locationsFoundIn.add(location);

//                logger.debug("Added location to " + this.getSpeciesName() + ": " + location.getCountry());
            } else {
                logger.debug(location.getCountry() + " already exists in " + this.getSpeciesName() + "!");
            }
        }
    }

    // Adds multiple locations to the tree species by iterating over a list
    public void addMultipleLocations(List<Locations> locations){
        locations.forEach(this::addLocation);
    }

    // Adds a single tree to the tree species
    public void addTree(Tree tree){
        if (!(this.conservationStatus == ConservationStatus.EXTINCT)) {
            if (!this.treesInSpecies.contains(tree)) {
                this.treesInSpecies.add(tree);

//                logger.debug("Added new tree to " + this.getSpeciesName() + ": " + tree.getTreeID());
            } else {
                logger.debug(tree.getTreeID() + " already exists in " + this.getSpeciesName() + "!");
            }
        }
    }

    // Adds multiple trees to the species by iterating over a list
    public void addMultipleTrees(List<Tree> trees){
        trees.forEach(this::addTree);
    }

    // ---------GETTERS & SETTERS-------- //

    public List<Locations> getLocationsFoundIn() {
        return locationsFoundIn;
    }

    public String getSpeciesName() {
        return speciesName;
    }

    public void setSpeciesName(String speciesName) {
        this.speciesName = speciesName;
    }

    public LeafType getLeafType() {
        return leafType;
    }

    public void setLeafType(LeafType leafType) {
        this.leafType = leafType;
    }

    public ConservationStatus getConservationStatus() {
        return conservationStatus;
    }

    public void setConservationStatus(ConservationStatus conservationStatus) {
        this.conservationStatus = conservationStatus;
    }

    public boolean bearsFruit() {
        return bearsFruit;
    }

    public void setBearsFruit(boolean bearsFruit) {
        this.bearsFruit = bearsFruit;
    }

    public List<Tree> getTreesInSpecies() {
        return treesInSpecies;
    }

    public int getSpeciesId() {
        return speciesId;
    }

// -------------TOSTRING------------- //

    @Override
    public String toString() {
        List<String> countryList = new ArrayList<>();
        for (Locations location : locationsFoundIn) {
            countryList.add(location.getCountry());
        }

        if (countryList.isEmpty()){
            countryList.add("nowhere");
        }

        return String.format("%s trees are in: %s. Leaves are: %s. Conservation status: %s. It %s fruit.",
                speciesName, String.join(", ", countryList), leafType.getTypeString(), conservationStatus, bearsFruit ? "bears" : "does not bear");
    }
}
