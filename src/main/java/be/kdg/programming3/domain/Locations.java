package be.kdg.programming3.domain;

import jakarta.persistence.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name="location")
public class Locations {
    @Transient
    private final Logger logger = LoggerFactory.getLogger(Locations.class);

    // ---------------------------------- //

    // Used from TreeSpecies
    @ManyToMany(cascade = {CascadeType. DETACH, CascadeType. MERGE,
            CascadeType.PERSIST, CascadeType. REFRESH}, mappedBy = "locationsFoundIn")
    private List<TreeSpecies> speciesFound;
    // Add transient to list definition not to work with console application

    // ---------------------------------- //

    // Used from Tree
    @OneToMany(mappedBy = "location", cascade = CascadeType.ALL)
    private List<Tree> treesInLocation;
    // Add transient to list definition not to work with console application

    // ---------------------------------- //

    public enum Climate {
        TUNDRA("Tundra"), SUBTROPICAL("Subtropical"), TEMPERATE("Temperate"),
        TROPICAL("Tropical"), POLAR("Polar"), ALPINE("Alpine"), DESERT("Desert"),
        MARITIME("Maritime"), CONTINENTAL("Continental"), MARS("Mars");

        public final String climate;

        Climate(String label) {
            this.climate = label;
        }

        public String getClimateString() {
            return climate;
        }
    }

    @Enumerated(EnumType.STRING)
    @Convert(converter = ClimateArrayConverter.class)
    private Climate[] subClimates;

    @Column(name = "yearly_average_rainfall")
    private double yearlyAverageRainfall;

    @Column(name = "percentage_of_forestland")
    private int percentageOfForestland;
    private String country;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "location_id")
    private int locationId;

    // ------------CONSTRUCTOR----------- //

    protected Locations(){}

    public Locations(String country, List<TreeSpecies> speciesFound, Climate[] subClimates, double yearlyAverageRainfall, int percentageOfForestland) {
        this.country = country;
        this.speciesFound = speciesFound;
        this.subClimates = subClimates;
        this.yearlyAverageRainfall = yearlyAverageRainfall;
        this.percentageOfForestland = percentageOfForestland;
        this.treesInLocation = new ArrayList<>();

//        logger.debug("Created location with name: " + country + "; species found: " + speciesFound + "; sub climates: " + Arrays.toString(subClimates) + "; yearly rainfall: " + yearlyAverageRainfall + "; % of forest coverage: " + percentageOfForestland);
    }

    public Locations(String country, Climate[] subClimates, double yearlyAverageRainfall, int percentageOfForestland) {
        this.country = country;
        this.subClimates = subClimates;
        this.yearlyAverageRainfall = yearlyAverageRainfall;
        this.percentageOfForestland = percentageOfForestland;
        this.speciesFound = new ArrayList<>();
        this.treesInLocation = new ArrayList<>();

//        logger.debug("Created location with name: " + country + "; sub climates: " + Arrays.toString(subClimates) + "; yearly rainfall: " + yearlyAverageRainfall + "; % of forest coverage: " + percentageOfForestland);
    }

    public Locations(int locationId, String country, Climate[] subClimates, double yearlyAverageRainfall, int percentageOfForestland) {
        this.locationId = locationId;
        this.country = country;
        this.subClimates = subClimates;
        this.yearlyAverageRainfall = yearlyAverageRainfall;
        this.percentageOfForestland = percentageOfForestland;
        this.speciesFound = new ArrayList<>();
        this.treesInLocation = new ArrayList<>();

//        logger.debug("Created location with name: " + country + "; sub climates: " + Arrays.toString(subClimates) + "; yearly rainfall: " + yearlyAverageRainfall + "; % of forest coverage: " + percentageOfForestland);
    }


    // ---------------------------------- //

    // Adds a single species to the location
    public void addSpecies(TreeSpecies species){
        if (!this.speciesFound.contains(species)){
            this.speciesFound.add(species);

//            logger.debug("Added new species to " + this.getCountry() + ": " + species.getSpeciesName());
        } else {
            logger.debug(species.getSpeciesName() + " already exists in " + this.getCountry() + "!");
        }
    }

    public void addMultipleSpecies(List<TreeSpecies> species){
        species.forEach(this::addSpecies);
    }

    // Adds a single tree to the location
    public void addTree(Tree tree){
        if(!this.treesInLocation.contains(tree)){
            this.treesInLocation.add(tree);

//            logger.debug("Added new tree to " + this.getCountry() + ": " + tree.getTreeID());
        } else {
            logger.debug("Tree " + tree.getTreeID() + " already exists in " + this.getCountry() + "!");
        }
    }

    // Adds multiple trees to the location by iterating over a list of trees
    public void addMultipleTrees(List<Tree> trees){
        trees.forEach(tree -> {
            if(!this.treesInLocation.contains(tree)){
                this.treesInLocation.add(tree);

//                logger.debug("Added new tree to " + this.getCountry() + ": " + tree.getTreeID());
            } else {
                logger.debug("Tree " + tree.getTreeID() + " already exists in " + this.getCountry() + "!");
            }
        });
    }

// ---------GETTERS & SETTERS-------- //

    public int getLocationId() {
        return locationId;
    }

    public List<TreeSpecies> getSpeciesFound() {
        return speciesFound;
    }

    public void setSpeciesFound(List<TreeSpecies> speciesFound) {
        this.speciesFound = speciesFound;
    }

    public Climate[] getSubClimates() {
        return subClimates;
    }

    public void setSubClimates(Climate[] subClimates) {
        this.subClimates = subClimates;
    }

    // Gets the array of subclimates formatted into a string separated by commas
    public String getFormattedSubClimates() {
        return Arrays.stream(subClimates)
                .map(Climate::getClimateString)
                .collect(Collectors.joining(", "));
    }

    public double getYearlyAverageRainfall() {
        return yearlyAverageRainfall;
    }

    public void setYearlyAverageRainfall(double yearlyAverageRainfall) {
        this.yearlyAverageRainfall = yearlyAverageRainfall;
    }

    public int getPercentageOfForestland() {
        return percentageOfForestland;
    }

    public void setPercentageOfForestland(int percentageOfForestland) {
        this.percentageOfForestland = percentageOfForestland;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Tree> getTreesInLocation() {
        return treesInLocation;
    }

    public void setTreesInLocation(List<Tree> treesInLocation) {
        this.treesInLocation = treesInLocation;
    }
// -------------TOSTRING------------- //


    @Override
    public String toString() {
        List<String> speciesList = new ArrayList<>();
        for (TreeSpecies species : speciesFound) {
            speciesList.add(species.getSpeciesName());
        }

        if (speciesList.isEmpty()){
            speciesList.add("none");
        }

        return String.format("%d : %s has %s tree(s). Sub climates: %s. Yearly rainfall: %.2f mm. Forest coverage: %d%%.",
                locationId, country, String.join(", ", speciesList), Arrays.toString(subClimates), yearlyAverageRainfall, percentageOfForestland);
    }

    // Converts an array of subclimates into a variable that is interpretable by the database and the other way around
    @Converter
    public static class ClimateArrayConverter implements AttributeConverter<Locations.Climate[], String> {

        @Override
        public String convertToDatabaseColumn(Locations.Climate[] attribute) {
            return Arrays.stream(attribute)
                    .map(Locations.Climate::name)
                    .collect(Collectors.joining(","));
        }

        @Override
        public Locations.Climate[] convertToEntityAttribute(String dbData) {
            return Arrays.stream(dbData.split(","))
                    .map(Locations.Climate::valueOf)
                    .toArray(Locations.Climate[]::new);
        }
    }
}
