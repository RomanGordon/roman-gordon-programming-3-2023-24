package be.kdg.programming3.domain;

import jakarta.persistence.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

@Entity
@Table(name="tree")
public class Tree {
    @Transient
    private final Logger logger = LoggerFactory.getLogger(Tree.class);

    // Used from TreeSpecies
    @ManyToOne(cascade = {CascadeType. DETACH,
            CascadeType.MERGE, CascadeType. PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "species_id")
    private TreeSpecies species;

    // ---------------------------------- //

    //Used from Locations
    @ManyToOne(cascade = {CascadeType. DETACH,
            CascadeType.MERGE, CascadeType. PERSIST,
            CascadeType.REFRESH})
    @JoinColumn(name = "location_id")
    private Locations location;

    // ---------------------------------- //

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tree_id")
    private int treeID;

    @Column(name = "planting_date")
    private LocalDate plantingDate;

    private double height;

    // ------------CONSTRUCTOR----------- //

    public Tree(int treeID, LocalDate plantingDate, TreeSpecies species, Locations location, double height) {
        this.treeID = treeID;
        this.plantingDate = plantingDate;
        this.species = species;
        this.location = location;
        this.height = height;

//        logger.debug("Created new Tree with ID: " + treeID + "; planting date: " + plantingDate + "; species: " + species.getSpeciesName() + "; location: " + location.getCountry() + "; height: " + height);
    }

    public Tree() {}


    // ---------GETTERS & SETTERS-------- //

    public TreeSpecies getSpecies() {
        return species;
    }

    public void setSpecies(TreeSpecies species) {
        this.species = species;
    }

    public Locations getLocation() {
        return location;
    }

    public void setLocation(Locations location) {
        this.location = location;
    }

    public int getTreeID() {
        return treeID;
    }

    public LocalDate getPlantingDate() {
        return plantingDate;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    // -------------TOSTRING------------- //

    @Override
    public String toString() {
        return String.format("Tree %d; Planted: %s(%d years old). Location: %s. Species: %s. Height: %.2f m",
                treeID, plantingDate, LocalDate.now().getYear() - plantingDate.getYear(), location.getCountry(), species.getSpeciesName(), height);
    }
}
