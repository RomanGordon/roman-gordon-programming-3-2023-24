package be.kdg.programming3.service.treeSpecies;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.TreeSpecies;

import java.util.List;

public interface TreeSpeciesService {
    TreeSpecies addSpecies(String speciesName, TreeSpecies.LeafType leafType, TreeSpecies.ConservationStatus conservationStatus, boolean bearsFruit);

    TreeSpecies addSpecies(String speciesName, TreeSpecies.LeafType leafType, TreeSpecies.ConservationStatus conservationStatus, List<Locations> locations, boolean bearsFruit);

    TreeSpecies addSpeciesLocations(TreeSpecies species, List<Locations> locations);

    List<TreeSpecies> getSpecies();

    List<TreeSpecies> getFilteredSpecies(int status);
    void writeTreeSpeciesToJson(List<TreeSpecies> treeSpecies);
    TreeSpecies getSpeciesByName(String name);
    void removeSpecies(String species);
    void updateSpecies(int speciesId, String speciesName, TreeSpecies.LeafType leafType, TreeSpecies.ConservationStatus conservationStatus, boolean bearsFruit);
}
