package be.kdg.programming3.service.treeSpecies;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.TreeSpecies;
import be.kdg.programming3.domain.TreeSpecies.*;
import be.kdg.programming3.repository.JSonWriter;
import be.kdg.programming3.repository.treeSpecies.TreeSpeciesJPARepository;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("jparepository")
public class TreeSpeciesJPAService implements TreeSpeciesService{
    private final Logger logger = LoggerFactory.getLogger(TreeSpeciesService.class);
    private final TreeSpeciesJPARepository treeSpeciesRepository;
    private final JSonWriter jSonWriter;

    public TreeSpeciesJPAService(TreeSpeciesJPARepository treeSpeciesRepository, JSonWriter jSonWriter) {
        this.treeSpeciesRepository = treeSpeciesRepository;
        this.jSonWriter = jSonWriter;
    }

    @Override
    public TreeSpecies addSpecies(String speciesName, TreeSpecies.LeafType leafType, TreeSpecies.ConservationStatus conservationStatus, boolean bearsFruit) {
        TreeSpecies species = new TreeSpecies(speciesName, leafType, conservationStatus, bearsFruit);
        treeSpeciesRepository.save(species);
        logger.debug("Saved tree species without locations to repository: " + species);
        return species;
    }

    @Override
    public TreeSpecies addSpecies(String speciesName, TreeSpecies.LeafType leafType, TreeSpecies.ConservationStatus conservationStatus, List<Locations> locations, boolean bearsFruit) {
        TreeSpecies species = new TreeSpecies(speciesName, leafType, conservationStatus, locations, bearsFruit);
        treeSpeciesRepository.save(species);
        logger.debug("Saved tree species with locations to repository: " + species);
        return species;
    }

    @Override
    public TreeSpecies addSpeciesLocations(TreeSpecies species, List<Locations> locations) {
        locations.forEach(species::addLocation);
        treeSpeciesRepository.save(species);
        logger.debug("Added the following locations to " + species.getSpeciesName() + ": " + locations);
        return species;
    }

    @Override
    public List<TreeSpecies> getSpecies() {
        return treeSpeciesRepository.findAll();
    }

    @Override
    public List<TreeSpecies> getFilteredSpecies(int status) {
        return treeSpeciesRepository.findAllByConservationStatusEquals(ConservationStatus.values()[status-1]);
    }

    @Override
    public void writeTreeSpeciesToJson(List<TreeSpecies> treeSpecies) {
        jSonWriter.saveToJson(treeSpecies, "treeSpecies");
    }

    @Override
    public TreeSpecies getSpeciesByName(String name) {
        try {
            return treeSpeciesRepository.findBySpeciesNameEqualsIgnoreCase(name).get(0);
        } catch (IndexOutOfBoundsException e){
            logger.warn("Unable to find species in database!");
            throw new DataAccessException("No such species") {};
        }
    }

    @Override
    @Transactional
    public void removeSpecies(String species) {
        treeSpeciesRepository.deleteBySpeciesNameEqualsIgnoreCase(species);
    }

    @Override
    @Transactional
    public void updateSpecies(int speciesId, String speciesName, LeafType leafType, ConservationStatus conservationStatus, boolean bearsFruit) {
        TreeSpecies species = treeSpeciesRepository.findById(speciesId).orElse(null);
        if (species == null) throw new DataAccessException("No such species") {};

        species.setSpeciesName(speciesName);
        species.setLeafType(leafType);
        species.setConservationStatus(conservationStatus);
        species.setBearsFruit(bearsFruit);

        treeSpeciesRepository.save(species);
    }
}
