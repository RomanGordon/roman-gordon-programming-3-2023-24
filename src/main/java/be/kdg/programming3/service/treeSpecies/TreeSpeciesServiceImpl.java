package be.kdg.programming3.service.treeSpecies;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.TreeSpecies;
import be.kdg.programming3.domain.TreeSpecies.ConservationStatus;
import be.kdg.programming3.domain.TreeSpecies.LeafType;
import be.kdg.programming3.repository.JSonWriter;
import be.kdg.programming3.repository.treeSpecies.TreeSpeciesRepository;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile({"jpadatabase", "jdbcdatabase", "javacollections"})
public class TreeSpeciesServiceImpl implements TreeSpeciesService {
    private final Logger logger = LoggerFactory.getLogger(TreeSpeciesService.class);
    private final TreeSpeciesRepository treeSpeciesRepository;
    private final JSonWriter jSonWriter;

    public TreeSpeciesServiceImpl(TreeSpeciesRepository treeSpeciesRepository, JSonWriter jSonWriter) {
        this.treeSpeciesRepository = treeSpeciesRepository;
        this.jSonWriter = jSonWriter;
    }

    @Override
    public TreeSpecies addSpecies(String speciesName, LeafType leafType, ConservationStatus conservationStatus, boolean bearsFruit){
        return treeSpeciesRepository.createSpecies(new TreeSpecies(speciesName, leafType, conservationStatus, bearsFruit));
    }

    @Override
    public TreeSpecies addSpecies(String speciesName, LeafType leafType, ConservationStatus conservationStatus, List<Locations> locations, boolean bearsFruit){
        return treeSpeciesRepository.createSpecies(new TreeSpecies(speciesName, leafType, conservationStatus, locations, bearsFruit));
    }

    @Override
    public TreeSpecies addSpeciesLocations(TreeSpecies species, List<Locations> locations){
        return treeSpeciesRepository.addSpeciesLocations(species, locations);
    }

    @Override
    public List<TreeSpecies> getSpecies() {
        return treeSpeciesRepository.readSpecies();
    }

    @Override
    public List<TreeSpecies> getFilteredSpecies(int status) {
        logger.debug("Filtering by tree species");
        return getSpecies().stream().filter(treeSpecies -> treeSpecies.getConservationStatus() == ConservationStatus.values()[status-1]).toList();
    }

    @Override
    public void writeTreeSpeciesToJson(List<TreeSpecies> treeSpecies) {
        jSonWriter.saveToJson(treeSpecies, "treeSpecies");
    }

    @Override
    public TreeSpecies getSpeciesByName(String name) {
        return treeSpeciesRepository.getSpeciesByName(name);
    }

    @Override
    public void removeSpecies(String species){
        treeSpeciesRepository.removeSpecies(species);
    }

    @Override
    @Transactional
    public void updateSpecies(int speciesId, String speciesName, LeafType leafType, ConservationStatus conservationStatus, boolean bearsFruit) {
        TreeSpecies species = treeSpeciesRepository.readSpecies().stream().filter(treeSpecies -> treeSpecies.getSpeciesId() == speciesId).toList().get(0);
        if (species == null) throw new DataAccessException("No such species") {};

        species.setSpeciesName(speciesName);
        species.setLeafType(leafType);
        species.setConservationStatus(conservationStatus);
        species.setBearsFruit(bearsFruit);

        treeSpeciesRepository.updateSpecies(species);
    }
}
