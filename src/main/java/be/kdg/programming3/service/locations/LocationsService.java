package be.kdg.programming3.service.locations;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.Tree;
import be.kdg.programming3.domain.TreeSpecies;

import java.util.List;

public interface LocationsService {
    Locations addLocation(String country, Locations.Climate[] subClimates, double yearlyAverageRainfall, int percentageOfForestland);

    Locations addLocation(String country, List<TreeSpecies> treeSpecies, Locations.Climate[] subClimates, double yearlyAverageRainfall, int percentageOfForestland);

    Locations addLocationSpecies(Locations locations, List<TreeSpecies> species);

    List<Locations> getLocations();

    List<Locations> getFilteredLocations(String minForest, String minRainfall);
    void writeLocationsToJson(List<Locations> locations);

    Locations getLocationByName(String locName);

    void removeLocation(String country);

    void updateLocation(int locationId, String country, Locations.Climate[] subClimates, double yearlyAverageRainfall, int percentageOfForestland);

    List<Locations> findWhereMinForestCoverage(String forestCoverage);

    List<Locations> findWhereRainfallBelow(String rainfall);

    List<Locations> findWhereNumberOfSpeciesAbove(String number);
}
