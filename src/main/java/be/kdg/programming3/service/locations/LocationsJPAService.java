package be.kdg.programming3.service.locations;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.TreeSpecies;
import be.kdg.programming3.repository.JSonWriter;
import be.kdg.programming3.repository.locations.LocationsJPARepository;
import be.kdg.programming3.util.exception.InvalidValueException;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("jparepository")
public class LocationsJPAService implements LocationsService{
    private final Logger logger = LoggerFactory.getLogger(LocationsService.class);

    private final LocationsJPARepository locationsRepository;
    private final JSonWriter jSonWriter;

    public LocationsJPAService(LocationsJPARepository locationsRepository, JSonWriter jSonWriter){
        this.locationsRepository = locationsRepository;
        this.jSonWriter = jSonWriter;
    }

    @Override
    public Locations addLocation(String country, Locations.Climate[] subClimates, double yearlyAverageRainfall, int percentageOfForestland) {
        Locations location = new Locations(country, subClimates, yearlyAverageRainfall, percentageOfForestland);
        locationsRepository.save(location);
        logger.debug("Saved location without TreeSpecies to repository: " + location);
        return location;
    }

    @Override
    public Locations addLocation(String country, List<TreeSpecies> treeSpecies, Locations.Climate[] subClimates, double yearlyAverageRainfall, int percentageOfForestland) {
        Locations location = new Locations(country, treeSpecies, subClimates, yearlyAverageRainfall, percentageOfForestland);
        locationsRepository.save(location);
        logger.debug("Saved location with TreeSpecies to repository: " + location);
        return location;
    }

    @Override
    public Locations addLocationSpecies(Locations locations, List<TreeSpecies> species) {
        species.forEach(locations::addSpecies);
        locationsRepository.save(locations);
        logger.debug("Added the following species to " + locations.getCountry() + ": " + species);
        return locations;
    }

    @Override
    public List<Locations> getLocations() {
        return locationsRepository.findAll();
    }

    @Override
    public List<Locations> getFilteredLocations(String minForest, String minRainfall) {
        try {
            int forest = Integer.parseInt(minForest);
            double rainfall = Double.parseDouble(minRainfall);

            if (minForest.isEmpty() && !minRainfall.isEmpty()){
                return locationsRepository.findByYearlyAverageRainfallGreaterThanEqual(rainfall);
            } else if (!minForest.isEmpty() && minRainfall.isEmpty()){
                return locationsRepository.findByPercentageOfForestlandGreaterThanEqual(forest);
            } else {
                return locationsRepository.findByPercentageOfForestlandGreaterThanEqualAndYearlyAverageRainfallGreaterThanEqual(forest, rainfall);
            }
        } catch (NumberFormatException e){
            logger.warn("Unable to find matching countries in database!");
            throw new DataAccessException("No such country") {};
        }
    }

    @Override
    public void writeLocationsToJson(List<Locations> locations) {
        jSonWriter.saveToJson(locations, "locations");
    }

    @Override
    public Locations getLocationByName(String locName) {
        try {
            return locationsRepository.findByCountryEqualsIgnoreCase(locName).get(0);
        } catch (IndexOutOfBoundsException e){
            logger.warn("Unable to find country in database!");
            throw new DataAccessException("No such country") {};
        }
    }

    @Override
    @Transactional
    public void removeLocation(String country) {
        Locations location = locationsRepository.findByCountryEqualsIgnoreCase(country).get(0);

        for (TreeSpecies species : location.getSpeciesFound()) {
            species.getLocationsFoundIn().remove(location);
        }

        location.getSpeciesFound().clear();

        locationsRepository.deleteByCountryEqualsIgnoreCase(country);
    }

    @Override
    public void updateLocation(int locationId, String country, Locations.Climate[] subClimates, double yearlyAverageRainfall, int percentageOfForestland) {
        Locations location = locationsRepository.findById(locationId).orElse(null);
        if (location == null) throw new DataAccessException("No such country") {};

        location.setCountry(country);
        location.setSubClimates(subClimates);
        location.setYearlyAverageRainfall(yearlyAverageRainfall);
        location.setPercentageOfForestland(percentageOfForestland);

        locationsRepository.save(location);
    }

    @Override
    public List<Locations> findWhereMinForestCoverage(String forestCoverage){
        try {
            int coverage = Integer.parseInt(forestCoverage);
            if (coverage > 100) {
                logger.warn("Forest coverage can not be over 100%!");
                throw new InvalidValueException("Forest High");
            }
            if (coverage < 0) {
                logger.warn("Forest coverage can not be below 0%!");
                throw new InvalidValueException("Forest Low");
            }

            return locationsRepository.findByPercentageOfForestlandGreaterThanEqual(coverage);
        } catch (NumberFormatException e){
            logger.warn("Invalid value for forest coverage!");
            throw new InvalidValueException("Forest Invalid");
        }
    }

    @Override
    public List<Locations> findWhereRainfallBelow(String rainfall){
        try {
            double rain = Double.parseDouble(rainfall);
            if (rain < 0) {
                logger.warn("Rainfall can not be below 0mm!");
                throw new InvalidValueException("Rainfall Low");
            }

            return locationsRepository.findByYearlyAverageRainfallLessThanEqual(rain);
        } catch (NumberFormatException e){
            logger.warn("Invalid value for rainfall!");
            throw new InvalidValueException("Rainfall Invalid");
        }
    }

    @Override
    public List<Locations> findWhereNumberOfSpeciesAbove(String number){
        try {
            int species = Integer.parseInt(number);
            if (species < 0) {
                logger.warn("You can't have less than 0 species!");
                throw new InvalidValueException("Species Low");
            }

            return locationsRepository.findWhereMinimumTreeSpecies(species);
        } catch (NumberFormatException e){
            logger.warn("Invalid value for number of species!");
            throw new InvalidValueException("Species Invalid");
        }
    }
}
