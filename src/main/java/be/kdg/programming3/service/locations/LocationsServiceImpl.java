package be.kdg.programming3.service.locations;

import be.kdg.programming3.domain.Locations;
import be.kdg.programming3.domain.Locations.Climate;
import be.kdg.programming3.domain.Tree;
import be.kdg.programming3.domain.TreeSpecies;
import be.kdg.programming3.repository.JSonWriter;
import be.kdg.programming3.repository.locations.LocationsRepository;
import be.kdg.programming3.util.exception.InvalidValueException;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile({"jpadatabase", "jdbcdatabase", "javacollections"})
public class LocationsServiceImpl implements LocationsService {
    private final Logger logger = LoggerFactory.getLogger(LocationsService.class);

    private final LocationsRepository locationsRepository;
    private JSonWriter jSonWriter;

    public LocationsServiceImpl(LocationsRepository locationsRepository, JSonWriter jSonWriter){
        this.locationsRepository = locationsRepository;
        this.jSonWriter = jSonWriter;
    }

    @Override
    public Locations addLocation(String country, Climate[] subClimates, double yearlyAverageRainfall, int percentageOfForestland){
        return locationsRepository.createLocation(new Locations(country, subClimates, yearlyAverageRainfall, percentageOfForestland));
    }

    @Override
    public Locations addLocation(String country, List<TreeSpecies> treeSpecies, Climate[] subClimates, double yearlyAverageRainfall, int percentageOfForestland){
        return locationsRepository.createLocation(new Locations(country, treeSpecies, subClimates, yearlyAverageRainfall, percentageOfForestland));
    }

    @Override
    public Locations addLocationSpecies(Locations locations, List<TreeSpecies> species){
        return locationsRepository.addLocationSpecies(locations, species);
    }

    @Override
    public List<Locations> getLocations() {
        return locationsRepository.readLocations();
    }

    @Override
    public List<Locations> getFilteredLocations(String minForest, String minRainfall) {
        List<Locations> filteredLocations = getLocations();
        logger.debug("Filter locations based on criteria.");
        try {
            logger.debug("Attempting to filter by forest coverage.");
            int minForestInt = Integer.parseInt(minForest);
            if(!minForest.trim().isEmpty()){
                filteredLocations = filteredLocations.stream().filter(locations -> locations.getPercentageOfForestland() > minForestInt).toList();
            }
        } catch (NumberFormatException e){
            System.out.println("Invalid input for forest coverage!");

            logger.debug("Failed to filter for forest coverage");
        }

        try {
            logger.debug("Attempting to filter by rainfall");
            double minRainfallDouble = Double.parseDouble(minRainfall);
            if(!minRainfall.trim().isEmpty()){
                filteredLocations = filteredLocations.stream().filter(locations -> locations.getYearlyAverageRainfall() > minRainfallDouble).toList();
            }
        } catch (NumberFormatException e){
            System.out.println("Invalid input for rainfall!");

            logger.debug("Failed to filter by rainfall");
        }

        if(minForest.trim().isEmpty() && minRainfall.trim().isEmpty()){
            filteredLocations = getLocations();
            logger.debug("Nothing to filter by");
        }
        return filteredLocations;
    }

    @Override
    public void writeLocationsToJson(List<Locations> locations) {
        jSonWriter.saveToJson(locations, "locations");
    }

    @Override
    public Locations getLocationByName(String locName) {
        return locationsRepository.getLocationByName(locName);
    }

    @Override
    public void removeLocation(String country){
        locationsRepository.removeLocation(country);
    }

    @Override
    @Transactional
    public void updateLocation(int locationId, String country, Climate[] subClimates, double yearlyAverageRainfall, int percentageOfForestland) {
        Locations location = locationsRepository.readLocations().stream().filter(locations -> locations.getLocationId() == locationId).toList().get(0);
        if (location == null) throw new DataAccessException("No such country") {};

        location.setCountry(country);
        location.setSubClimates(subClimates);
        location.setYearlyAverageRainfall(yearlyAverageRainfall);
        location.setPercentageOfForestland(percentageOfForestland);

        locationsRepository.updateLocation(location);
    }


    @Override
    public List<Locations> findWhereMinForestCoverage(String forestCoverage){
        try {
            int coverage = Integer.parseInt(forestCoverage);
            if (coverage > 100) {
                logger.warn("Forest coverage can not be over 100%!");
                throw new InvalidValueException("Forest High");
            }
            if (coverage < 0) {
                logger.warn("Forest coverage can not be below 0%!");
                throw new InvalidValueException("Forest Low");
            }

            return locationsRepository.findWhereMinForestCoverage(coverage);
        } catch (NumberFormatException e){
            logger.warn("Invalid value for forest coverage!");
            throw new InvalidValueException("Forest Invalid");
        }
    }

    @Override
    public List<Locations> findWhereRainfallBelow(String rainfall){
        try {
            double rain = Double.parseDouble(rainfall);
            if (rain < 0) {
                logger.warn("Rainfall can not be below 0mm!");
                throw new InvalidValueException("Rainfall Low");
            }

            return locationsRepository.findWhereRainfallBelow(rain);
        } catch (NumberFormatException e){
            logger.warn("Invalid value for rainfall!");
            throw new InvalidValueException("Rainfall Invalid");
        }
    }

    @Override
    public List<Locations> findWhereNumberOfSpeciesAbove(String number){
        try {
            int species = Integer.parseInt(number);
            if (species < 0) {
                logger.warn("You can't have less than 0 species!");
                throw new InvalidValueException("Species Low");
            }

            return locationsRepository.findWhereNumberOfSpeciesAbove(species);
        } catch (NumberFormatException e){
            logger.warn("Invalid value for number of species!");
            throw new InvalidValueException("Species Invalid");
        }
    }
}
