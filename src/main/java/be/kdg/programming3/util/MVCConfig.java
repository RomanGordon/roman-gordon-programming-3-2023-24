package be.kdg.programming3.util;

import be.kdg.programming3.domain.TreeSpecies.*;
import be.kdg.programming3.presentation.converters.ListStringToArrayClimateConverter;
import be.kdg.programming3.presentation.converters.StringToConservationStatusConverter;
import be.kdg.programming3.presentation.converters.StringToEnumGenericConverter;
import be.kdg.programming3.presentation.converters.StringToLeafTypeConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.List;
import java.util.Locale;

@Configuration
public class MVCConfig implements WebMvcConfigurer {
    @Bean
    public LocaleResolver localeResolver() { // So that you can use ?lang=fr
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.ENGLISH);
        return slr;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }

    @Override
    public void addFormatters(FormatterRegistry registry){
        // registry.addConverter(new CONVERTER)
        registry.addConverter(new ListStringToArrayClimateConverter());
//        registry.addConverter(new StringToConservationStatusConverter());
//        registry.addConverter(new StringToLeafTypeConverter());
        registry.addConverter(new StringToEnumGenericConverter<>(ConservationStatus.class));
        registry.addConverter(new StringToEnumGenericConverter<>(LeafType.class));
    }
}
